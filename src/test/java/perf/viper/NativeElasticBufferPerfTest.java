package perf.viper;

import net.viper.core.memory.ElasticBuffer;
import net.viper.core.memory.internal.NativeElasticBuffer;
import net.viper.core.memory.pooling.buffers.OnDemandBufferRecycler;

public class NativeElasticBufferPerfTest {
    static ElasticBuffer buffer;

    public static void main(String[] args) {
        buffer = new NativeElasticBuffer(OnDemandBufferRecycler.PORTION_INSTANCE);
        measureNativeElasticBuffer();
    }

    public static void measureNativeElasticBuffer() {
        long nums = (1L << 32); // 2^32 byte

        long startTime, endTime;

        startTime = System.currentTimeMillis();

        System.out.println("Write 2^32 bytes (~4Gb) to buffer");
        for (long i = 0; i < nums; i++) {
            buffer.put(i, (byte) i);
        }
        endTime = System.currentTimeMillis();

        long elapsedTime = endTime - startTime;
        int opsPerMs = (int) (nums / elapsedTime);
        System.out.printf("Put operation elapsed_time = %s; ops/ms = %s%n", elapsedTime, opsPerMs);

        startTime = System.currentTimeMillis();
        for (long i = 0; i < nums; i++) {
            buffer.get(i);
        }
        endTime = System.currentTimeMillis();
        elapsedTime = endTime - startTime;
        opsPerMs = (int) (nums / elapsedTime);
        System.out.printf("Get operation elapsed_time = %s; ops/ms = %s%n", elapsedTime, opsPerMs);
    }
}

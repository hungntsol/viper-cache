package perf.viper;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.List;

public class BenchmarkKryoRunner {
    public static void main(String[] args) throws IOException {
        org.openjdk.jmh.Main.main(args);
    }

    static List<TestInnerClass> listInner = List.of(new TestInnerClass(1), new TestInnerClass(2));
    static TestClassInstance testInstance = new TestClassInstance(1, "test-1", new TestInnerClass(1), listInner);

    @Benchmark
    @BenchmarkMode({Mode.Throughput, Mode.Throughput})
    public void benchmarkKryoWriteWithoutRegistration() {
        Kryo kryo = new Kryo();
        kryo.setRegistrationRequired(false);

        try (
                Output output = new Output(1024, -1)
        ) {
            kryo.writeClassAndObject(output, testInstance);
        }
    }

    @Benchmark
    @BenchmarkMode({Mode.Throughput, Mode.Throughput})
    public void benchmarkKryoWriteRegistration() {
        Kryo kryo = new Kryo();
        kryo.setRegistrationRequired(false);

        kryo.register(TestClassInstance.class);
        kryo.register(TestInnerClass.class);

        try (
                Output output = new Output(1024, -1)
        ) {
            kryo.writeClassAndObject(output, testInstance);
        }
    }

    private static class TestClassInstance {
        int id;
        String name;
        TestInnerClass innerClass;
        List<TestInnerClass> testInnerClassList;

        public TestClassInstance(int id, String name, TestInnerClass innerClass,
                                 List<TestInnerClass> testInnerClassList) {
            this.id = id;
            this.name = name;
            this.innerClass = innerClass;
            this.testInnerClassList = testInnerClassList;
        }
    }

    private static class TestInnerClass {
        long power;

        public TestInnerClass(long power) {
            this.power = power;
        }
    }
}

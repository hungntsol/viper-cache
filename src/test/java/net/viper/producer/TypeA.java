package net.viper.producer;

public class TypeA {
    int id;
    String name;
    boolean working;
    TypeB typeB;

    public TypeA() {}

    public TypeA(int id, String name, boolean working, TypeB typeB) {
        this.id = id;
        this.name = name;
        this.working = working;
        this.typeB = typeB;
    }
}

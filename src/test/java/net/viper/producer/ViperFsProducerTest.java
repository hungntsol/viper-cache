package net.viper.producer;

import com.esotericsoftware.kryo.Kryo;
import net.viper.consumer.ViperConsumer;
import net.viper.consumer.fs.ViperFsBlobFetcher;
import net.viper.consumer.internal.ViperConsumerController;
import net.viper.consumer.internal.ViperConsumerStatesContext;
import net.viper.consumer.internal.ViperDataUpdater;
import net.viper.consumer.read.ViperStatesReadEngine;
import net.viper.core.memory.MemoryMode;
import net.viper.core.memory.pooling.buffers.OnDemandBufferRecycler;
import net.viper.producer.internal.ViperProducerBaseImpl;
import net.viper.producer.internal.ViperProducerStatesContext;
import net.viper.producer.internal.ViperVersionSequenceMinter;
import net.viper.producer.fs.ViperFsBlobStagger;
import net.viper.producer.fs.ViperFsPublisher;
import net.viper.utils.logic.Randoms;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ViperFsProducerTest {

    private static final Logger LOG = Logger.getLogger(ViperFsProducerTest.class.getName());


    @Test
    public void producerAddObject() {
        Path publishDirPath = Paths.get("src", "test", "resources", "publish-dir");
        File publishDir = publishDirPath.toFile();
        publishDir.mkdir();

        LOG.info("PRODUCER publish to dir = " + publishDir.getAbsolutePath());

        ViperProducer producer = ViperProducer.fromBuilder()
                .withBlobPublisher(new ViperFsPublisher(publishDirPath))
                .withBlobCleaner(ViperProducer.BlobCleaner.EMPTY_CLEANER)
                .withBlobStagger(new ViperFsBlobStagger(ViperProducer.BlobCompressor.EMPTY_COMPRESSOR))
                .withVersionMinter(new ViperVersionSequenceMinter())
                .build();

        producer.registerClassResolver(Map.of(99, TypeA.class, 101, TypeB.class));
        producer.registerModel(List.of(TypeA.class));

        producer.produce(task -> {
            for (int i = 0; i < 100_000; i++) {
                TypeA sampleObj = new TypeA(2, "test-" + i, false, new TypeB("test-b", 2));
                task.addObject(sampleObj.name, sampleObj);
            }

            for (int i = 0; i < 10; i++) {
                String name = "test-" + Randoms.randInt(0, 100);
                task.remove(name, TypeA.class);
            }
        });

        producer.produce(task -> {
            for (int i = 100_00; i < 100_500; i++) {
                TypeA sampleObj = new TypeA(2, "test-" + i, false, new TypeB("test-b", 2));
                task.addObject(sampleObj.name, sampleObj);
            }
        });

        producer.produce(task -> {
            for (int i = 150_000; i < 200_000; i++) {
                TypeA sampleObj = new TypeA(2, "test-" + i, false, new TypeB("test-b", 2));
                task.addObject(sampleObj.name, sampleObj);
            }
        });


        // consume
        ViperConsumerStatesContext consumerStatesContext = new ViperConsumerStatesContext(new Kryo(),
                OnDemandBufferRecycler.PORTION_INSTANCE);
        ViperStatesReadEngine consumerReadEngine = new ViperStatesReadEngine(consumerStatesContext,
                OnDemandBufferRecycler.PORTION_INSTANCE);
        ViperConsumerController consumerController = new ViperConsumerController(
                new ViperDataUpdater(new ViperFsBlobFetcher(publishDirPath)),
                MemoryMode.ON_HEAP,
                consumerReadEngine);

        try {
            consumerController.update(new ViperConsumer.VersionInformation(3));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}

package net.viper.producer;

public class TypeB {
    String name;
    long value;

    public TypeB() {

    }

    public TypeB(String name, long value) {
        this.name = name;
        this.value = value;
    }
}

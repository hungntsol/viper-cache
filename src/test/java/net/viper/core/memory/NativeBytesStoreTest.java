package net.viper.core.memory;

import net.viper.core.memory.pooling.buffers.OnDemandBufferRecycler;
import net.viper.core.memory.store.NativeBytesStore;
import org.junit.Assert;
import org.junit.Test;

public class NativeBytesStoreTest {
    NativeBytesStore store = new NativeBytesStore(OnDemandBufferRecycler.PORTION_INSTANCE);

    @Test
    public void writeAndRead() {
        int nums = 258;

        for (int i = 0; i < nums; i++) {
            store.write((byte) i);
        }

        for (int i = 0; i < nums; i++) {
            if (((byte) i ) != store.read(i))
                Assert.fail();
        }
    }

    @Test
    public void clear_shouldPositionZero() {
        int nums = 128;
        for (int i = 0; i < nums; i++) {
            store.write((byte) i);
        }

        Assert.assertEquals(nums, store.position());

        store.seek(10);
        Assert.assertEquals(10, store.position());

        store.clear();
        Assert.assertEquals(0, store.position());
    }
}

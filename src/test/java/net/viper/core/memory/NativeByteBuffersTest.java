package net.viper.core.memory;

import net.viper.core.memory.internal.NativeElasticBuffer;
import net.viper.core.memory.pooling.buffers.OnDemandBufferRecycler;
import net.viper.core.memory.pooling.bytes.OnDemandBytesRecycler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

public class NativeByteBuffersTest {
    ElasticBuffer buffer;

    @Before
    public void before() {
        buffer = new NativeElasticBuffer(OnDemandBufferRecycler.PORTION_INSTANCE);
    }

    @Test
    public void putAndGet() {
        int nums = 128;
        for (int i = 0; i < nums; i++) {
            buffer.put(i, (byte) i);
        }

        for (int i = 0; i < nums; i++) {
            if ((byte) i != buffer.get(i))
                Assert.fail();
        }
    }

    @Test
    public void growBufferSize() {
        int nums = 600;
        for (int i = 0; i < nums; i++) {
            buffer.put(i, (byte) i);
        }

        for (int i = 0; i < nums; i++) {
            if ((byte) i != buffer.get(i))
                Assert.fail();
        }

        long currLen = buffer.capacity();

        // assert grow 2 time from SMALL_INSTANCE (log2 = 10, chunk = 8)
        Assert.assertEquals(1024, currLen);
    }

    @Test
    public void appendBuffer() {
        int nums = (1 << 12);
        for (int i = 0; i < nums; i++) {
            buffer.put(i, (byte) i);
        }

        long currCap = buffer.capacity();
        Assert.assertTrue((1 << 12) <= currCap);
    }

    @Test
    public void constructNativeBytesStore_givenOverLimit() {
        Assert.assertThrows(IllegalArgumentException.class, () ->
                new NativeElasticBuffer(new OnDemandBufferRecycler(31)));
    }

    @Test
    public void putAndGetBulk() {
        byte[] randomBytes = new byte[2000];
        Random rand = new Random();
        rand.nextBytes(randomBytes);

        buffer.put(0, randomBytes);

        byte[] retByteArr = buffer.get(0, 2000);
        Assert.assertArrayEquals(randomBytes, retByteArr);
    }
}

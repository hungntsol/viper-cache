package net.viper.core.memory;

import net.viper.core.memory.internal.SegmentedBytes;
import net.viper.core.memory.pooling.bytes.OnDemandBytesRecycler;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class SegmentedBytesTest {
    SegmentedBytes segmentedBytes = new SegmentedBytes(OnDemandBytesRecycler.PORTION_INSTANCE);

    @Test
    public void setAndGet() {
        int nums = 300;
        for (int i = 0; i < nums; i++) {
            segmentedBytes.set(i, (byte) i);
        }

        for (int i = 0; i < nums; i++) {
            if ((byte) i != segmentedBytes.get(i))
                Assert.fail();
        }
    }

    @Test
    public void copy() {
        int nums = 1024;
        for (int i = 0; i < nums; i++) {
            segmentedBytes.set(i, (byte) i);
        }

        SegmentedBytes copiedSegmented = new SegmentedBytes(OnDemandBytesRecycler.PORTION_INSTANCE);

        int lenToCopy = 256;
        copiedSegmented.copy(segmentedBytes, 100, 130, lenToCopy);

        for (int i = 0; i < lenToCopy; i++) {
            if (copiedSegmented.get(130 + i) != segmentedBytes.get(100 + i))
                Assert.fail();
        }
    }

    @Test
    public void orderedCopy() {
        int nums = 1024;
        for (int i = 0; i < nums; i++) {
            segmentedBytes.set(i, (byte) i);
        }

        SegmentedBytes copiedSegmented = new SegmentedBytes(OnDemandBytesRecycler.PORTION_INSTANCE);

        int lenToCopy = 256;
        copiedSegmented.orderedCopy(segmentedBytes, 100, 130, lenToCopy);

        for (int i = 0; i < lenToCopy; i++) {
            if (copiedSegmented.get(130 + i) != segmentedBytes.get(100 + i))
                Assert.fail();
        }
    }

    @Test
    public void orderedCopy_givenSrcBytes() {
        int nums = 2048;
        byte[] bytes = new byte[nums];

        new Random().nextBytes(bytes);

        segmentedBytes.orderedCopy(bytes, 0, 10, nums);

        for (int i = 0; i < nums; i++) {
            if (segmentedBytes.get(10 + i) != bytes[i])
                Assert.fail();
        }

        byte[] ret1024Bytes = segmentedBytes.get(0, 1024);
        for (int i = 0; i < ret1024Bytes.length; i++) {
            if (ret1024Bytes[i] != segmentedBytes.get(i))
                Assert.fail();
        }
    }

    @Test
    public void equalRange_shouldTrue() {
        int nums = 1024;
        for (int i = 0; i < nums; i++) {
            segmentedBytes.set(i, (byte) i);
        }

        SegmentedBytes copiedSegmented = new SegmentedBytes(OnDemandBytesRecycler.PORTION_INSTANCE);

        int lenToCopy = 256;
        copiedSegmented.orderedCopy(segmentedBytes, 100, 130, lenToCopy);

        boolean comparison = copiedSegmented.equalRange(130, segmentedBytes, 100, lenToCopy);
        Assert.assertTrue(comparison);
    }

    @Test
    public void equalRange_shouldFalse() {
        SegmentedBytes other = new SegmentedBytes(OnDemandBytesRecycler.PORTION_INSTANCE);
        int nums = 12;
        for (int i = 0; i < nums; i++) {
            segmentedBytes.set(i, (byte) i);
            other.set(i, (byte) i);
        }

        boolean comparison = other.equalRange(1, segmentedBytes, 2, 3);
        Assert.assertFalse(comparison);
    }
}

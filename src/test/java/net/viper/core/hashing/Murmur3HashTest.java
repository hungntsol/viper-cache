package net.viper.core.hashing;

import net.viper.utils.logic.Randoms;
import org.junit.Assert;
import org.junit.Test;

public class Murmur3HashTest {
    @Test
    public void hash_givenBytes() {
        byte[] bytes = Randoms.randBytes(35);
        int hashKey1 = Murmur3Hash.hash(bytes);
        int hashKey2 = Murmur3Hash.hash(bytes);

        Assert.assertEquals(hashKey1, hashKey2);
    }
}

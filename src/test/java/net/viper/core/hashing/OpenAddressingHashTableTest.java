package net.viper.core.hashing;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OpenAddressingHashTableTest {
    OpenAddressingHashTable<String> hashTable;

    @Before
    public void before() {
        hashTable = new OpenAddressingHashTable<>();
    }

    @Test
    public void put() {
        for (int i = 0; i < 100; i++) {
            hashTable.put("test-" + i, "test-" + i);
        }

        for (int i = 0; i < 100; i++) {
            Assert.assertTrue(hashTable.contains("test-" + i));
            Assert.assertEquals("test-" + i, hashTable.search("test-" + i));
        }

        hashTable.put("test-22", "update-test-22");
    }

    @Test
    public void remove() {
        for (int i = 0; i < 100; i++) {
            hashTable.put("test-" + i, "test-" + i);
        }

        hashTable.remove("test-10");
        hashTable.remove("test-11");

        Assert.assertFalse(hashTable.contains("test-10"));
        Assert.assertFalse(hashTable.contains("test-11"));
    }

    @Test
    public void resize() {
        OpenAddressingHashTable<String> resizedTable = new OpenAddressingHashTable<>(10, 0.5f);

        for (int i = 0; i < 20; i++) {
            String sample = "test-" + i;
            resizedTable.put(sample, sample);
        }

        Assert.assertEquals(40, resizedTable.capacity());
        Assert.assertEquals(20, resizedTable.size());
    }

    @Test
    public void putAndRemove() {
        for (int i = 0; i < 10_000; i++) {
            hashTable.put("test-" + i, "test-" + i);
            hashTable.remove("test-" + i);
        }

        for (int i = 0; i < 10_000; i++) {
            Assert.assertFalse(hashTable.contains("test-" + i));
//            Assert.assertEquals("test-" + i, hashTable.search("test-" + i));
        }

        System.out.println(hashTable.capacity());
        System.out.println(hashTable.size());
    }
}

package net.viper.producer.fs;

import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.ViperProducer;
import net.viper.utils.logic.IOs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

public class ViperFsPublisher implements ViperProducer.BlobPublisher {

    private static final Logger LOG = Logger.getLogger(ViperFsPublisher.class.getName());

    private final Path blobDirPath;

    public ViperFsPublisher(Path blobDirPath) {
        this.blobDirPath = blobDirPath;
        IOs.mkdirs(this.blobDirPath);
    }

    @Override
    public void publish(ViperProducer.PublishArtifact publishArtifact) {
        try {
            if (publishArtifact instanceof ViperProducer.HeaderBlob headerBlob)
                publishHeader(headerBlob);
            else if (publishArtifact instanceof ViperProducer.Blob blob)
                publishBlob(blob);
        } catch (IOException ex) {
            throw new ViperRuntimeException("Error in publish blob content", ex);
        }
    }

    private void publishHeader(ViperProducer.HeaderBlob headerBlob) throws IOException {
        Path destination = this.blobDirPath.resolve(String.format("header-%d", headerBlob.getVersion()));
        publishContent(headerBlob, destination);
    }

    private void publishBlob(ViperProducer.Blob blob) throws IOException {
        Path destination = this.blobDirPath.resolve(String.format("%s-%d-%d",
                blob.getBlobType().getPrefix(), blob.getFromVersion(), blob.getToVersion()));

        publishContent(blob, destination);
    }

    private void publishContent(ViperProducer.PublishArtifact publishArtifact, Path destination) throws IOException {
        long startTime = System.currentTimeMillis();
        try (
                InputStream is = publishArtifact.inputStream();
                OutputStream os = Files.newOutputStream(destination);
        ) {

            byte[] buf = new byte[4096];
            int n;

            while ((n = is.read(buf)) != -1)
                os.write(buf, 0, n);
        }

        long endTime = System.currentTimeMillis();
        LOG.info(String.format("BLOB publish; destination = %s; elapse_ms = %dms",
                destination.toAbsolutePath(), (endTime - startTime)));
    }
}

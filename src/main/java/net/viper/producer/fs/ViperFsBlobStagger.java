package net.viper.producer.fs;

import net.viper.Jvm;
import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.ViperProducer;
import net.viper.producer.write.ViperBlobWriter;
import net.viper.utils.logic.IOs;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class ViperFsBlobStagger implements ViperProducer.BlobStagger {
    private final Path stagingPath;
    private final ViperProducer.BlobCompressor compressor;

    public ViperFsBlobStagger(ViperProducer.BlobCompressor compressor) {
        this.stagingPath = Path.of(Jvm.SYSTEM_TEMPDIR);
        this.compressor = compressor;

        IOs.mkdirs(this.stagingPath);
    }

    @Override
    public ViperProducer.HeaderBlob openHeader(long version) {
        return new FsHeaderBlob(version, this.stagingPath, this.compressor);
    }

    @Override
    public ViperProducer.Blob openDelta(long fromVersion, long toVersion) {
        return new FsBlob(fromVersion, toVersion, this.stagingPath, this.compressor);
    }

    @Override
    public ViperProducer.Blob openReversedDelta(long fromVersion, long toVersion) {
        return new FsBlob(fromVersion, toVersion, this.stagingPath, this.compressor);
    }

    public static class FsHeaderBlob extends ViperProducer.HeaderBlob {
        private final ViperProducer.BlobCompressor compressor;

        protected FsHeaderBlob(long version, Path path, ViperProducer.BlobCompressor compressor) {
            super(version, path);
            this.compressor = compressor;
        }

        @Override
        public void write(ViperBlobWriter blobWriter) throws IOException {
            Path parent = this.path.getParent();
            IOs.mkdirs(parent);
            IOs.touch(this.path);

            try (
                    OutputStream os =
                            new BufferedOutputStream(this.compressor.compress(Files.newOutputStream(this.path)))
            ) {
                blobWriter.writeHeader(os);
            }

        }

        @Override
        public InputStream inputStream() throws IOException {
            return new BufferedInputStream(this.compressor.decompress(Files.newInputStream(this.path)));
        }

        @Override
        public void cleanup() {
            if (this.path != null) {
                try {
                    Files.deleteIfExists(this.path);
                } catch (IOException e) {
                    throw new ViperRuntimeException(e);
                }
            }
        }
    }

    public static class FsBlob extends ViperProducer.Blob {
        private final ViperProducer.BlobCompressor compressor;

        protected FsBlob(long fromVersion, long toVersion, Path dirPath, ViperProducer.BlobCompressor compressor) {
            super(fromVersion, toVersion, dirPath);
            this.compressor = compressor;
        }

        @Override
        public void write(ViperBlobWriter blobWriter) throws IOException {
            Path parentDir = this.path.getParent();

            IOs.mkdirs(parentDir);
            IOs.touch(this.path);

            try (
                    OutputStream os =
                            new BufferedOutputStream(this.compressor.compress(Files.newOutputStream(this.path)))
            ) {
                switch (blobType) {
                    case DELTA_JOURNAL -> blobWriter.writeDelta(os);
                    case REVERSED_DELTA_JOURNAL -> blobWriter.writeReversedDelta(os);
                    default -> throw new IllegalStateException("Unknown blob type");
                }
            }
        }

        @Override
        public InputStream inputStream() throws IOException {
            return new BufferedInputStream(this.compressor.decompress(Files.newInputStream(this.path)));
        }

        @Override
        public void cleanup() {
            if (this.path != null) {
                try {
                    Files.deleteIfExists(this.path);
                } catch (IOException e) {
                    throw new ViperRuntimeException(e);
                }
            }
        }
    }
}

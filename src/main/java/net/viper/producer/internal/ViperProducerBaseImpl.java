package net.viper.producer.internal;

public class ViperProducerBaseImpl extends ViperAbstractProducer {
    public ViperProducerBaseImpl(Builder builder) {
        super(builder);
    }

    @Override
    public long produce(Populator populator) {
        return runProduce(populator);
    }
}

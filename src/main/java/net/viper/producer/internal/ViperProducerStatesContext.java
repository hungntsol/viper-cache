package net.viper.producer.internal;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Registration;
import net.viper.core.memory.pooling.bytes.OnDemandBytesRecycler;
import net.viper.core.schema.ViperSchema;
import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.write.ViperSchemaStateWrite;
import net.viper.producer.write.ViperSchemaStateWriteImpl;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ViperProducerStatesContext {

    private static final Logger LOG = Logger.getLogger(ViperProducerStatesContext.class.getName());

    private final Kryo kryoInstance;
    private final Map<Integer, Class<?>> registeredResolverClasses;
    private final Map<String, ViperSchemaStateWrite> typeToStateWriteMap;

    public ViperProducerStatesContext(Kryo kryoInstance) {
        this.kryoInstance = kryoInstance;
        this.registeredResolverClasses = new ConcurrentHashMap<>();
        this.typeToStateWriteMap = new ConcurrentHashMap<>();
    }

    public void registerClassResolver(Map<Integer, Class<?>> registrations) {
        for (Map.Entry<Integer, Class<?>> entry : registrations.entrySet()) {
            Registration registration = this.kryoInstance.getClassResolver().getRegistration(entry.getValue());
            if (registration != null) {
                return;
            }

            Class<?> clazz = entry.getValue();
            int registrationId = entry.getKey();

            this.kryoInstance.register(clazz, registrationId);
            this.registeredResolverClasses.put(registrationId, clazz);
        }
    }

    public Map<Integer, Class<?>> getRegisteredResolverClasses() {
        return this.registeredResolverClasses;
    }

    public void registerModel(Type type) {
        ViperSchema schema = new ViperSchema(type.getTypeName());
        ViperSchemaStateWrite stateWrite = new ViperSchemaStateWriteImpl(OnDemandBytesRecycler.PORTION_INSTANCE,
                schema,
                this.kryoInstance);

        this.typeToStateWriteMap.putIfAbsent(schema.getClazzName(), stateWrite);
    }

    public Map<String, ViperSchemaStateWrite> getStateWriteMap() {
        return this.typeToStateWriteMap;
    }

    public Set<ViperSchema> getSchemas() {
        return this.typeToStateWriteMap.values()
                .stream()
                .map(ViperSchemaStateWrite::getSchema)
                .collect(Collectors.toSet());
    }

    public void addObject(String key, Object object) {
        ViperSchemaStateWrite stateWrite = findViperSchemaStateWrite(object.getClass());
        try {
            stateWrite.writeObject(key, object);
        } catch (Throwable cause) {
            LOG.severe(String.format("Error when write object; key = %s", key));
            throw cause;
        }
    }

    public void removeObject(String key, Class<?> clazz) {
        ViperSchemaStateWrite stateWrite = findViperSchemaStateWrite(clazz);

        try {
            stateWrite.removeObject(key);
        } catch (Throwable cause) {
            LOG.severe(String.format("Error when remove object; key = %s; type = %s", key, clazz.getTypeName()));
            throw cause;
        }
    }

    private ViperSchemaStateWrite findViperSchemaStateWrite(Type type) {
        ViperSchemaStateWrite stateWrite = this.typeToStateWriteMap.get(type.getTypeName());
        if (stateWrite == null)
            throw new ViperRuntimeException("Cannot find matched state-write");

        return stateWrite;
    }
}

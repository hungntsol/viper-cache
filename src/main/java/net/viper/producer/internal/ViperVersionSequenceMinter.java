package net.viper.producer.internal;

import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.ViperProducer;

import java.util.concurrent.atomic.AtomicLong;

public class ViperVersionSequenceMinter implements ViperProducer.VersionMinter {
    private static final long INITIAL_VALUE = 0;
    private static final AtomicLong versionCounter = new AtomicLong(INITIAL_VALUE);

    private boolean initialized = false;

    @Override
    public long mint() {
        return versionCounter.incrementAndGet();
    }

    @Override
    public long initiate() {
        if (initialized)
            throw new IllegalStateException("Cannot initiate value of version 2 times");

        this.initialized = true;
        return INITIAL_VALUE;
    }
}

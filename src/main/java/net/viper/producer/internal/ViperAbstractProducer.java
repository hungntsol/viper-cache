package net.viper.producer.internal;

import com.esotericsoftware.kryo.Kryo;
import net.viper.ViperConstants;
import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.ViperProducer;
import net.viper.producer.write.ViperBlobWriter;
import net.viper.producer.write.ViperBlobWriterImpl;
import net.viper.producer.write.ViperStatesWriteEngine;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ViperAbstractProducer implements ViperProducer {
    private static final Logger LOG = Logger.getLogger(ViperAbstractProducer.class.getName());

    private final VersionMinter versionMinter;
    private final BlobStagger blobStagger;
    private final BlobPublisher blobPublisher;
    private final BlobCleaner blobCleaner;
    private final ViperStatesWriteEngine statesWriteEngine;
    private final ViperProducerStatesContext statesContext;

    private final Lock singleProducerLock = new ReentrantLock();

    private PopulationAtomic atomic;

    private long lastSuccessVersion = ViperConstants.VERSION_NULL;

    protected ViperAbstractProducer(Builder builder) {
        this.versionMinter = builder.versionMinter;
        this.blobStagger = builder.blobStagger;
        this.blobPublisher = builder.blobPublisher;
        this.blobCleaner = builder.blobCleaner;

        this.statesContext = new ViperProducerStatesContext(new Kryo());
        this.statesWriteEngine = new ViperStatesWriteEngine(this.statesContext);
        this.atomic = PopulationAtomic.initDeltaChain(versionMinter.initiate());
    }

    @Override
    public void registerClassResolver(Map<Integer, Class<?>> registrations) {
        this.statesContext.registerClassResolver(registrations);
    }

    @Override
    public void registerModel(List<Type> types) {
        for (final Type type : types) {
            this.statesContext.registerModel(type);
        }
    }

    protected long runProduce(Populator task) {
        long toVersion = this.versionMinter.mint();
        Artifact artifact = new Artifact();

        long startTime = System.currentTimeMillis();

        try {
            // 1. setup state to write.
            this.statesWriteEngine.moveNextScope();

            // 2. population
            populateTask(task, toVersion);

            // 3. produce state if we have modification.
            if (this.statesWriteEngine.isModified()) {
                publish(artifact, toVersion);

                PopulationAtomic candidates = this.atomic.round(toVersion);
                candidates = checkout(candidates, artifact);

                try {
                    announce(candidates.getPending());
                    this.atomic = candidates.commit();
                } catch (Throwable cause) {
                    if (artifact.hasReversedDelta()) {
                        applyDelta(artifact.getReversedDeltaBlob());
                    }

                    throw cause;
                }

                this.lastSuccessVersion = toVersion;
            } else {
                this.statesWriteEngine.revertToLastState();
                // log revert action
            }
        } catch (Throwable cause) {
            try {
                this.statesWriteEngine.revertToLastState();
            } catch (Throwable innerCause) {
                LOG.log(Level.SEVERE, "Error when attempt to revert state engine to last state", innerCause);
                // swallow inner throwable
            }

            throw new ViperRuntimeException(cause);
        } finally {
            artifact.cleanup();

            long endTime = System.currentTimeMillis();
            LOG.info(String.format("[PRODUCER] produce a version; version = %d; elapsed_time = %dms",
                    toVersion, (endTime - startTime)));
        }

        return this.lastSuccessVersion;
    }

    void populateTask(Populator task, long toVersion) {
        try (
                CloseableProducerState statesContext = new CloseableProducerState(this.statesContext, toVersion)
        ) {
            task.populate(statesContext);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error in populate-task", e);
            throw new ViperRuntimeException(e);
        }
    }

    void publish(Artifact artifact, long toVersion) throws IOException {
        artifact.setHeaderBlob(this.blobStagger.openHeader(toVersion));
        stageAndPublishHeaderBlob(artifact.getHeaderBlob());

        // end if state not start.
        if (!this.atomic.hasCurrentState())
            return;

        artifact.setDeltaBlob(stageBlob(this.blobStagger.openDelta(
                this.atomic.getCurrent().getVersion(),
                toVersion)));
        artifact.setReversedDeltaBlob(stageBlob(this.blobStagger.openReversedDelta(
                toVersion,
                this.atomic.getCurrent().getVersion())));


        publishBlob(artifact.getDeltaBlob());
        publishBlob(artifact.getReversedDeltaBlob());
    }

    void announce(PopulationAtomic.Atomic atomic) {
        // TODO: announcer in ViperProducer.
    }

    void applyDelta(Blob blob) throws IOException {
        // TODO: apply delta to current engine state
    }

    private PopulationAtomic checkout(PopulationAtomic stateAtomic, Artifact artifact) throws IOException {
        PopulationAtomic result = stateAtomic;

        if (result.hasCurrentState()) {
            if (artifact.hasDelta()) {
                applyDelta(artifact.getDeltaBlob());
                result = stateAtomic.swap();
            }
        }

        return result;
    }

    private void stageAndPublishHeaderBlob(HeaderBlob headerBlob) throws IOException {
        ViperBlobWriter blobWriter = new ViperBlobWriterImpl(this.statesWriteEngine);
        headerBlob.write(blobWriter);

        this.blobPublisher.publish(headerBlob);
    }

    private Blob stageBlob(Blob blob) throws IOException {
        ViperBlobWriter blobWriter = new ViperBlobWriterImpl(this.statesWriteEngine);
        blob.write(blobWriter);

        return blob;
    }

    private void publishBlob(Blob blob) {
        try {
            this.blobPublisher.publish(blob);
        } finally {
            this.blobCleaner.clean(blob);
        }
    }
}

package net.viper.producer.internal;

import net.viper.ViperConstants;

public final class PopulationAtomic {
    private final Atomic current;
    private final Atomic pending;

    static PopulationAtomic initDeltaChain(long initVersion) {
        return new PopulationAtomic(atomic(initVersion), null);
    }

    static PopulationAtomic restore(Atomic state) {
        return new PopulationAtomic(state, null);
    }

    static Atomic atomic(long version) {
        return () -> version;
    }

    private PopulationAtomic(Atomic current, Atomic pending) {
        this.current = current;
        this.pending = pending;
    }

    Atomic getCurrent() {
        return current;
    }

    Atomic getPending() {
        return pending;
    }

    boolean hasCurrentState() {
        return getCurrent() != null;
    }

    long pendingVersion() {
        return this.pending == null ? ViperConstants.VERSION_NULL : this.pending.getVersion();
    }

    PopulationAtomic round(long pendingVersion) {
        if (this.pending != null)
            throw new IllegalArgumentException("Pending state is null");

        return new PopulationAtomic(this.current, atomic(pendingVersion));
    }

    PopulationAtomic commit() {
        if (this.pending == null)
            throw new IllegalArgumentException("Pending state is null");

        return new PopulationAtomic(this.pending, null);
    }

    PopulationAtomic rollback() {
        if (this.pending == null)
            throw new IllegalArgumentException("Pending state is null");

        return new PopulationAtomic(atomic(this.current.getVersion()), null);
    }

    PopulationAtomic swap() {
        return new PopulationAtomic(atomic(this.current.getVersion()),
                atomic(this.pending.getVersion()));
    }

    interface Atomic {
        long getVersion();
    }
}

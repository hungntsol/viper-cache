package net.viper.producer.internal;

import net.viper.exceptions.ViperRuntimeException;
import net.viper.producer.ViperProducer;

public class CloseableProducerState implements ViperProducer.StateWrite, AutoCloseable {
    private volatile boolean isClosed = false;

    private final ViperProducerStatesContext statesContext;
    private final long version;

    public CloseableProducerState(ViperProducerStatesContext statesContext, long version) {
        this.statesContext = statesContext;
        this.version = version;
    }

    @Override
    public void close() throws Exception {
        this.isClosed = true;
    }

    @Override
    public void addObject(String key, Object object) {
        ensureNotClosed();

        this.statesContext.addObject(key, object);
    }

    @Override
    public void remove(String key, Class<?> clazz) {
        ensureNotClosed();

        this.statesContext.removeObject(key, clazz);
    }

    @Override
    public long getVersion() {
        return this.version;
    }

    private void ensureNotClosed() {
        if (this.isClosed) {
            throw new ViperRuntimeException("Attempt to operate a closed context");
        }
    }
}

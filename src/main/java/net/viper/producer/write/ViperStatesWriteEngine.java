package net.viper.producer.write;

import net.viper.core.schema.ViperSchema;
import net.viper.core.states.ViperStatesEngine;
import net.viper.producer.internal.ViperProducerStatesContext;
import net.viper.utils.logic.Randoms;
import net.viper.utils.threads.ViperWorkerExecutor;

import java.util.*;

public class ViperStatesWriteEngine implements ViperStatesEngine {
    private final ViperProducerStatesContext statesContext;
    private long originRandomizedTag = -1L;
    private long destinationRandomizedTag;

    private StatePhase statePhase = StatePhase.NEXT_SCOPE;

    public ViperStatesWriteEngine(ViperProducerStatesContext statesContext) {
        this.statesContext = statesContext;
    }

    public void moveToWritePhase() {
        if (this.statePhase.equals(StatePhase.WRITE_PHASE))
            return;

        try (
                ViperWorkerExecutor executor = ViperWorkerExecutor.ofProcessor(getClass(), "state-write.engine.write")
        ) {
            for (final ViperSchemaStateWrite stateTable : this.statesContext.getStateWriteMap().values())
                executor.execute(stateTable::moveToWritePhase);
        }

        this.statePhase = StatePhase.WRITE_PHASE;
    }

    public void moveNextScope() {
        if (this.statePhase.equals(StatePhase.NEXT_SCOPE))
            return;

        this.originRandomizedTag = this.destinationRandomizedTag;
        this.destinationRandomizedTag = mintRandomizedTag();

        try (
                ViperWorkerExecutor executor = ViperWorkerExecutor.ofProcessor(getClass(), "state-write.engine.commit")
        ) {
            for (final ViperSchemaStateWrite stateTable : this.statesContext.getStateWriteMap().values())
                executor.execute(stateTable::moveNextScope);
        }

        this.statePhase = StatePhase.NEXT_SCOPE;
    }

    public void revertToLastState() {
        // TODO: implement revert
        throw new UnsupportedOperationException("not implement");
    }

    public boolean isModified() {
//        for (final Map.Entry<String, ViperStateTable> stateWriterEntry : this.stateTableMap.entrySet()) {
//            if (stateWriterEntry.getValue().isModified())
//                return true;
//        }

        // TODO: hardcode for test
        return true;
    }

    public long getOriginRandomizedTag() {
        return originRandomizedTag;
    }

    public long getDestinationRandomizedTag() {
        return destinationRandomizedTag;
    }

    public Map<String, ViperSchemaStateWrite> getStateTableMap() {
        return this.statesContext.getStateWriteMap();
    }

    public Set<ViperSchema> getSchemas() {
        return this.statesContext.getSchemas();
    }

    public ViperProducerStatesContext getStatesContext() {
        return this.statesContext;
    }

    private long mintRandomizedTag() {
        return Randoms.randLong();
    }

    private enum StatePhase {
        WRITE_PHASE,
        NEXT_SCOPE
    }
}

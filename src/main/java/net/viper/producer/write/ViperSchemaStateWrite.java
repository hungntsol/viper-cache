package net.viper.producer.write;

import net.viper.core.schema.ViperSchema;

import java.io.DataOutputStream;
import java.io.IOException;

public interface ViperSchemaStateWrite {
    ViperSchema getSchema();

    boolean isModified();

    void moveToWritePhase();

    void moveNextScope();

    /**
     * Use encoder to serialize object and remember data for write to blob.
     *
     * @param object object.
     */
    void writeObject(String key, Object object);

    void removeObject(String key);

    void prepareDeltaWrite();

    void writeDelta(DataOutputStream dos) throws IOException;

    void prepareReversedDeltaWrite();

    void writeReversedDelta(DataOutputStream dos) throws IOException;
}

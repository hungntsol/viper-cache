package net.viper.producer.write;

import net.viper.core.encoding.VarLenHandles;
import net.viper.core.objects.ViperBlobHeaderPlaceholder;
import net.viper.core.schema.ViperSchema;
import net.viper.utils.threads.ViperWorkerExecutor;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public final class ViperBlobWriterImpl implements ViperBlobWriter {

    private final ViperStatesWriteEngine statesWriteEngine;

    public ViperBlobWriterImpl(ViperStatesWriteEngine statesWriteEngine) {
        this.statesWriteEngine = statesWriteEngine;
    }

    @Override
    public void writeHeader(OutputStream os) throws IOException {
        this.statesWriteEngine.moveToWritePhase();

        DataOutputStream dos = new DataOutputStream(os);

        ViperBlobHeaderPlaceholder headerBlobPlaceholder = buildHeaderBlob(this.statesWriteEngine.getSchemas(), false);
        ViperBlobHeaderWriterExtension.writeHeader(headerBlobPlaceholder, dos);
        ViperBlobHeaderWriterExtension.writeHeaderClassRegistrations(dos,
                this.statesWriteEngine.getStatesContext().getRegisteredResolverClasses());

        os.flush();
    }

    @Override
    public void writeDelta(OutputStream os) throws IOException {
        this.statesWriteEngine.moveToWritePhase();

        DataOutputStream dos = new DataOutputStream(os);

        // TODO: @@@ write delta-schema changes
        // Set<ViperSchema> changedSchema = extractChangedSchema();

        // 1. BLOB write randomized_tag, schema_size (long, long, int)
        ViperBlobHeaderPlaceholder headerPlaceholder = buildHeaderBlob(this.statesWriteEngine.getSchemas(), false);
        dos.writeLong(headerPlaceholder.getOriginRandomizedTag());
        dos.writeLong(headerPlaceholder.getDestinationRandomizedTag());
        VarLenHandles.writeVarInt(dos, headerPlaceholder.getManagedSchemas().size());

        try (
                ViperWorkerExecutor executor = ViperWorkerExecutor.ofProcessor(getClass(), "blob-writer-impl.delta")
        ) {
            for (final ViperSchemaStateWrite stateWriter : this.statesWriteEngine.getStateTableMap().values()) {
                executor.execute(stateWriter::prepareDeltaWrite);
            }
        }

        for (final ViperSchemaStateWrite stateWriter : this.statesWriteEngine.getStateTableMap().values()) {
            stateWriter.writeDelta(dos);
        }

        os.flush();
    }

    @Override
    public void writeReversedDelta(OutputStream os) throws IOException {
        this.statesWriteEngine.moveToWritePhase();

        DataOutputStream dos = new DataOutputStream(os);

        ViperBlobHeaderPlaceholder headerPlaceholder = buildHeaderBlob(this.statesWriteEngine.getSchemas(), true);
        dos.writeLong(headerPlaceholder.getOriginRandomizedTag());
        dos.writeLong(headerPlaceholder.getDestinationRandomizedTag());

        try (
                ViperWorkerExecutor executor = ViperWorkerExecutor.ofProcessor(getClass(),
                        "blob-writer-impl.reversed_delta")
        ) {
            for (final ViperSchemaStateWrite stateWrite : this.statesWriteEngine.getStateTableMap().values())
                executor.execute(stateWrite::prepareReversedDeltaWrite);
        }

        for (final ViperSchemaStateWrite stateWrite : this.statesWriteEngine.getStateTableMap().values())
            stateWrite.writeReversedDelta(dos);


        os.flush();
    }

    private ViperBlobHeaderPlaceholder buildHeaderBlob(
            Collection<ViperSchema> schemas,
            boolean isReversed) {
        ViperBlobHeaderPlaceholder headerPlaceholder = new ViperBlobHeaderPlaceholder();
        if (isReversed) {
            headerPlaceholder.setOriginRandomizedTag(this.statesWriteEngine.getDestinationRandomizedTag());
            headerPlaceholder.setDestinationRandomizedTag(this.statesWriteEngine.getOriginRandomizedTag());
        } else {
            headerPlaceholder.setOriginRandomizedTag(this.statesWriteEngine.getOriginRandomizedTag());
            headerPlaceholder.setDestinationRandomizedTag(this.statesWriteEngine.getDestinationRandomizedTag());
        }

        headerPlaceholder.setManagedSchemas(schemas);
        return headerPlaceholder;
    }

    private Set<ViperSchema> extractChangedSchema() {
        // TODO: implement
        return this.statesWriteEngine.getSchemas();
    }
}

package net.viper.producer.write;

import java.io.IOException;
import java.io.OutputStream;

public interface ViperBlobWriter {
    void writeHeader(OutputStream os) throws IOException;

    void writeDelta(OutputStream os) throws IOException;

    void writeReversedDelta(OutputStream os) throws IOException;
}

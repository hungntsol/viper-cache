package net.viper.producer.write;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.unsafe.UnsafeOutput;
import net.viper.core.encoding.VarLenHandles;
import net.viper.core.hashing.OpenAddressingHashTable;
import net.viper.core.memory.pooling.bytes.BytesMemoryRecycler;
import net.viper.core.memory.store.BytesStore;
import net.viper.core.schema.ViperSchema;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class ViperSchemaStateWriteImpl implements ViperSchemaStateWrite {

    private static final Logger LOG = Logger.getLogger(ViperSchemaStateWriteImpl.class.getName());

    protected final BytesStore toAddMessageStore;
    protected final BytesStore toAddHeaderStore;

    protected final BytesStore toRemovalMessageStore;
    protected final BytesStore toRemovalHeaderStore;

    protected final ViperSchema schema;
    protected final Kryo encoder;

    // TODO: @@@ currently use hashcode to for previous and current state.
    // in later, must implement another way to compare
    protected Set<Integer> currentAddingPopulationHashCodes = new HashSet<>();
    protected Set<Integer> previousAddingPopulationHashCode = new HashSet<>();

    protected final OpenAddressingHashTable<byte[]> stateTable = new OpenAddressingHashTable<>();

    private final UnsafeOutput unsafeOutput = new UnsafeOutput(4096, -1);

    private boolean inReversedDelta = false;

    public ViperSchemaStateWriteImpl(
            BytesMemoryRecycler memoryRecycler,
            ViperSchema schema,
            Kryo encoder) {
        this.toAddMessageStore = new BytesStore(memoryRecycler);
        this.toAddHeaderStore = new BytesStore(memoryRecycler);
        this.toRemovalMessageStore = new BytesStore(memoryRecycler);
        this.toRemovalHeaderStore = new BytesStore(memoryRecycler);
        this.schema = schema;
        this.encoder = encoder;
    }

    @Override
    public ViperSchema getSchema() {
        return this.schema;
    }

    @Override
    public boolean isModified() {
        return !this.currentAddingPopulationHashCodes.equals(this.previousAddingPopulationHashCode);
    }

    @Override
    public void moveToWritePhase() {

    }

    @Override
    public void moveNextScope() {
        this.previousAddingPopulationHashCode.clear();
        this.previousAddingPopulationHashCode.addAll(this.currentAddingPopulationHashCodes);
        this.currentAddingPopulationHashCodes.clear();

        this.toAddMessageStore.clear();
        this.toAddHeaderStore.clear();
        this.toRemovalMessageStore.clear();
        this.toRemovalHeaderStore.clear();
    }

    /**
     * Use encoder to serialize object and remember data for write to blob.
     *
     * @param object object.
     */
    @Override
    public void writeObject(String key, Object object) {
        this.encoder.writeObject(this.unsafeOutput, object);
        byte[] objectDataBytes = this.unsafeOutput.toBytes();

        boolean mustWrite = false;
        if (!this.stateTable.contains(key)) {
            // key not put yet
            mustWrite = true;
        } else {
            // TODO: @@@ now only use default extractor (This is not optimize) => need another way.
            if (this.schema.isDefaultExtractor()) {
                // Object data is modified.
                mustWrite = !Arrays.equals(this.stateTable.search(key), objectDataBytes);
            }
        }

        if (!mustWrite)
            return;

        this.stateTable.put(key, objectDataBytes);

        long msgPosition = this.toAddMessageStore.position();

        // 4.a BLOB message length_of_key
        VarLenHandles.writeVarInt(this.toAddMessageStore, key.length());
        // 4.b BLOB message key
        this.toAddMessageStore.write(key.getBytes());

        // 4.c BLOB message length_of_object
        VarLenHandles.writeVarInt(this.toAddMessageStore, objectDataBytes.length);
        // 4.d BLOB message object
        this.toAddMessageStore.write(objectDataBytes);

        // write journal, used to retrieve data from message store.
        int writtenLength = (int) (this.toAddMessageStore.position() - msgPosition);
        VarLenHandles.writeVarLong(this.toAddHeaderStore, msgPosition);
        VarLenHandles.writeVarInt(this.toAddHeaderStore, writtenLength);

        this.unsafeOutput.reset();
    }

    @Override
    public void removeObject(String key) {
        byte[] dataBytes = this.stateTable.remove(key);
        if (dataBytes == null)
            return; // skipping, cause non-existed key in table.

        long msgPosition = this.toRemovalMessageStore.position();

        // 5.a BLOB message length_of_key
        VarLenHandles.writeVarInt(this.toRemovalMessageStore, key.length());
        // 5.b BLOB message key
        this.toRemovalMessageStore.write(key.getBytes());

        VarLenHandles.writeVarInt(this.toRemovalMessageStore, dataBytes.length);
        this.toRemovalMessageStore.write(dataBytes);

        int writtenLength = (int) (this.toRemovalMessageStore.position() - msgPosition);
        VarLenHandles.writeVarLong(this.toRemovalHeaderStore, msgPosition);
        VarLenHandles.writeVarInt(this.toRemovalHeaderStore, writtenLength);
    }

    @Override
    public void prepareDeltaWrite() {
    }

    @Override
    public void writeDelta(DataOutputStream dos) throws IOException {
        this.inReversedDelta = false;
        writeBlobContent(dos);
    }

    @Override
    public void prepareReversedDeltaWrite() {
    }

    @Override
    public void writeReversedDelta(DataOutputStream dos) throws IOException {
        this.inReversedDelta = true;
        writeBlobContent(dos);
    }

    private void writeBlobContent(DataOutputStream dos) throws IOException {
        // 2. BLOB write schema typename
        dos.writeUTF(this.schema.getClazzName());

        if (!this.inReversedDelta) {
            writeBlobAddingContent(dos, this.toAddHeaderStore, this.toAddMessageStore);
            writeBlobRemovalContent(dos, this.toRemovalHeaderStore, this.toRemovalMessageStore);
        } else {
            writeBlobAddingContent(dos, this.toRemovalHeaderStore, this.toRemovalMessageStore);
            writeBlobRemovalContent(dos, this.toAddHeaderStore, this.toAddMessageStore);
        }
    }

    private static void writeBlobAddingContent(DataOutputStream dos, BytesStore headerStore, BytesStore messageStore)
            throws IOException {
        int readPointer = 0;
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        while (readPointer < headerStore.position()) {
            long msgPosition = VarLenHandles.readVarLong(headerStore, readPointer);
            readPointer += VarLenHandles.sizeOfVarLong(msgPosition);
            int msgLength = VarLenHandles.readVarInt(headerStore, readPointer);
            readPointer += VarLenHandles.sizeOfVarInt(msgLength);

            // 4. BLOB write add_messages
            VarLenHandles.writeVarInt(byteOutputStream, msgLength);
            byteOutputStream.write(messageStore.read(msgPosition, msgLength));
        }

        // 3. BLOB write schema message size (varint)
        VarLenHandles.writeVarInt(dos, byteOutputStream.size());
        dos.write(byteOutputStream.toByteArray());
    }

    private static void writeBlobRemovalContent(DataOutputStream dos, BytesStore headerStore,
                                                BytesStore messageStore) throws IOException {
        int readPointer = 0;
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        while (readPointer < headerStore.position()) {
            // read message position
            long msgPosition = VarLenHandles.readVarLong(headerStore, readPointer);
            readPointer += VarLenHandles.sizeOfVarLong(msgPosition);
            // read message length
            int msgLength = VarLenHandles.readVarInt(headerStore, readPointer);
            readPointer += VarLenHandles.sizeOfVarInt(msgLength);


            // read key from message store.
            int messageKeyLength = VarLenHandles.readVarInt(messageStore, msgPosition);
            long messageKeyPosition = msgPosition + VarLenHandles.sizeOfVarInt(messageKeyLength);
            byte[] messageKey = messageStore.read(messageKeyPosition, messageKeyLength);

            // 5. BLOB write remove_message (only contains key)
            VarLenHandles.writeVarInt(byteOutputStream, messageKeyLength);
            byteOutputStream.write(messageKey);
        }

        VarLenHandles.writeVarInt(dos, byteOutputStream.size());
        dos.write(byteOutputStream.toByteArray());
    }
}

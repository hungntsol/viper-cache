package net.viper.producer.write;

import net.viper.core.encoding.VarLenHandles;
import net.viper.core.objects.ViperBlobHeaderPlaceholder;
import net.viper.core.schema.ViperSchema;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

class ViperBlobHeaderWriterExtension {
    static void writeHeader(ViperBlobHeaderPlaceholder placeholder, DataOutputStream dataStream) throws IOException {
        // header version id (4 byte)
        dataStream.writeInt(ViperBlobHeaderPlaceholder.BLOB_HEADER_VERSION_ID);

        // randomized tags (16 byte)
        dataStream.writeLong(placeholder.getOriginRandomizedTag());
        dataStream.writeLong(placeholder.getDestinationRandomizedTag());

        // schemas (varint, fixed length)
        ByteArrayOutputStream schemaByteArrStream = new ByteArrayOutputStream();
        VarLenHandles.writeVarInt(schemaByteArrStream, placeholder.getManagedSchemas().size());
        for (final ViperSchema schema : placeholder.getManagedSchemas()) {
            schema.write(schemaByteArrStream);
        }
        byte[] schemeOutputBytes = schemaByteArrStream.toByteArray();

        VarLenHandles.writeVarInt(dataStream, schemeOutputBytes.length + 1);
        dataStream.write(schemeOutputBytes);

        // backward compatibility, will be skipped when reading.
        VarLenHandles.writeVarInt(dataStream, 0);
    }

    static void writeHeaderClassRegistrations(DataOutputStream dos, Map<Integer, Class<?>> clazzRegistrations) throws IOException {
        // registrations
        VarLenHandles.writeVarInt(dos, clazzRegistrations.size());

        for (final Map.Entry<Integer, Class<?>> registration : clazzRegistrations.entrySet()) {
            VarLenHandles.writeVarInt(dos, registration.getKey());
            dos.writeUTF(registration.getValue().getTypeName());
        }
    }
}

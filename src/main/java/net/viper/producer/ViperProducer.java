package net.viper.producer;

import net.viper.core.objects.BlobType;
import net.viper.core.objects.ViperVersioningBlob;
import net.viper.core.objects.ViperStreamBlob;
import net.viper.producer.internal.ViperAbstractProducer;
import net.viper.producer.internal.ViperProducerBaseImpl;
import net.viper.producer.write.ViperBlobWriter;
import net.viper.utils.logic.Randoms;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface ViperProducer {

    long produce(Populator populator);

    void registerClassResolver(Map<Integer, Class<?>> registrations);

    void registerModel(List<Type> types);

    interface BlobPublisher {
        void publish(ViperProducer.PublishArtifact publishArtifact);
    }

    final class Artifact {
        private ViperProducer.HeaderBlob headerBlob = null;
        private ViperProducer.Blob deltaBlob = null;
        private ViperProducer.Blob reversedDeltaBlob = null;

        private boolean cleanupCalled = false;

        public synchronized void cleanup() {
            if (this.cleanupCalled)
                return;

            this.cleanupCalled = true;

            if (hasDelta()) {
                this.deltaBlob.cleanup();
                this.deltaBlob = null;
            }

            if (hasReversedDelta()) {
                this.reversedDeltaBlob.cleanup();
                this.reversedDeltaBlob = null;
            }
        }

        public boolean hasDelta() {
            return this.deltaBlob != null;
        }

        public boolean hasReversedDelta() {
            return this.reversedDeltaBlob != null;
        }

        public HeaderBlob getHeaderBlob() {
            return headerBlob;
        }

        public void setHeaderBlob(HeaderBlob headerBlob) {
            this.headerBlob = headerBlob;
        }

        public Blob getDeltaBlob() {
            return deltaBlob;
        }

        public void setDeltaBlob(Blob deltaBlob) {
            this.deltaBlob = deltaBlob;
        }

        public Blob getReversedDeltaBlob() {
            return reversedDeltaBlob;
        }

        public void setReversedDeltaBlob(Blob reversedDeltaBlob) {
            this.reversedDeltaBlob = reversedDeltaBlob;
        }
    }

    abstract class HeaderBlob implements PublishArtifact {
        protected final long version;
        protected final Path path;

        protected HeaderBlob(long version, Path path) {
            this.version = version;
            this.path = path.resolve(String.format("header-%d.%s", version, Randoms.randInt()));
        }

        public long getVersion() {
            return version;
        }

    }

    abstract class Blob extends ViperVersioningBlob implements PublishArtifact {
        protected final Path path;

        protected Blob(long fromVersion, long toVersion, Path path) {
            super(fromVersion, toVersion);

            BlobType blobType = getBlobType();
            switch (blobType) {
                case DELTA_JOURNAL -> this.path = path.resolve(String.format("%s-%d-%d",
                        blobType.getPrefix(), fromVersion, toVersion));
                case REVERSED_DELTA_JOURNAL -> this.path = path.resolve(String.format("%s-%d-%d",
                        blobType.getPrefix(), toVersion, fromVersion));
                default -> throw new IllegalStateException("Unknown blob type");
            }
        }

        public Path getPath() {
            return this.path;
        }
    }

    interface BlobCleaner {
        BlobCleaner EMPTY_CLEANER = new BlobCleaner() {
            @Override
            public void clean(Blob blob) {
                // nothing
            }
        };

        void clean(Blob blob);
    }

    interface BlobStagger {
        ViperProducer.HeaderBlob openHeader(long version);

        ViperProducer.Blob openDelta(long fromVersion, long toVersion);

        ViperProducer.Blob openReversedDelta(long fromVersion, long toVersion);
    }

    interface BlobCompressor {
        BlobCompressor EMPTY_COMPRESSOR = new BlobCompressor() {
            @Override
            public OutputStream compress(OutputStream os) {
                return os;
            }

            @Override
            public InputStream decompress(InputStream is) {
                return is;
            }
        };

        OutputStream compress(OutputStream os);

        InputStream decompress(InputStream is);
    }

    interface PublishArtifact extends ViperStreamBlob {
        void write(ViperBlobWriter blobWriter) throws IOException;

        void cleanup();

        default String getFilePath() {
            throw new UnsupportedOperationException("Default implementation not support");
        }
    }

    interface VersionMinter {
        long mint();

        long initiate();
    }

    @FunctionalInterface
    interface Populator {
        void populate(StateWrite context);
    }

    interface StateWrite {
        void addObject(String key, Object obj);

        void remove(String key, Class<?> clazz);

        long getVersion();
    }

    static Builder fromBuilder() {
        return new Builder();
    }


    class Builder {
        public BlobPublisher blobPublisher;
        public BlobCleaner blobCleaner;
        public BlobStagger blobStagger;
        public BlobCompressor blobCompressor;
        public VersionMinter versionMinter;
        public File stagingDir;

        Builder withBlobPublisher(BlobPublisher blobPublisher) {
            this.blobPublisher = blobPublisher;
            return this;
        }

        Builder withBlobCleaner(BlobCleaner blobCleaner) {
            this.blobCleaner = blobCleaner;
            return this;
        }

        Builder withBlobStagger(BlobStagger blobStagger) {
            this.blobStagger = blobStagger;
            return this;
        }

        Builder withBlobCompressor(BlobCompressor blobCompressor) {
            this.blobCompressor = blobCompressor;
            return this;
        }

        Builder withVersionMinter(VersionMinter versionMinter) {
            this.versionMinter = versionMinter;
            return this;
        }

        Builder withStagingDirectory(File dir) {
            this.stagingDir = dir;
            return this;
        }

        public ViperProducer build() {
            // TODO: perform build here.
            return new ViperProducerBaseImpl(this);
        }
    }
}

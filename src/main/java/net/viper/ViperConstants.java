package net.viper;

public class ViperConstants {
    public static final int MEGABYTE = 1024 * 1024;
    public static final int GIGABYTE = 1024 * MEGABYTE;

    public static final long VERSION_NULL = 0L;
    public static final long VERSION_LAST = Long.MAX_VALUE;
}

package net.viper.core.memory.store;

import net.viper.core.memory.Bytes;
import net.viper.core.memory.internal.SegmentedBytes;
import net.viper.core.memory.pooling.bytes.BytesMemoryRecycler;

public class BytesStore implements Bytes {
    private final SegmentedBytes buffer;
    private long position = 0;

    public BytesStore(BytesMemoryRecycler memoryRecycler) {
        this.buffer = new SegmentedBytes(memoryRecycler);
    }

    @Override
    public void write(byte b) {
        this.buffer.set(this.position++, b);
    }

    @Override
    public void write(byte[] bytes) {
        this.buffer.orderedCopy(bytes, 0, this.position, bytes.length);
        this.position += bytes.length;
    }

    @Override
    public byte read() {
        return this.buffer.get(this.position);
    }

    @Override
    public byte read(long position) {
        return this.buffer.get(position);
    }

    @Override
    public byte[] read(long position, int len){
        byte[] result = this.buffer.get(position, len);
        this.position += len;

        return result;
    }

    @Override
    public long position() {
        return this.position;
    }

    @Override
    public void seek(long position) {
        this.position = position;
    }

    @Override
    public void clear() {
        this.position = 0;
    }
}

package net.viper.core.memory.store;

import net.viper.core.memory.Bytes;
import net.viper.core.memory.ElasticBuffer;
import net.viper.core.memory.internal.NativeElasticBuffer;
import net.viper.core.memory.pooling.buffers.BufferRecycler;

public class NativeBytesStore implements Bytes {
    private final ElasticBuffer elasticBuffer;
    private long position = 0;

    public static NativeBytesStore elasticAllocate(BufferRecycler recycler) {
        return new NativeBytesStore(recycler);
    }

    public NativeBytesStore(BufferRecycler recycler) {
        this.elasticBuffer = new NativeElasticBuffer(recycler);
    }

    @Override
    public void write(byte b) {
        this.elasticBuffer.put(position++, b);
    }

    @Override
    public void write(byte[] bytes) {
        this.elasticBuffer.put(this.position, bytes);
        this.position += bytes.length;
    }

    @Override
    public byte read() {
        return this.elasticBuffer.get(this.position);
    }

    @Override
    public byte read(long pos) {
        return this.elasticBuffer.get(pos);
    }

    @Override
    public byte[] read(long pos, int len) {
        return this.elasticBuffer.get(pos, len);
    }

    @Override
    public void clear() {
        this.position = 0;
    }

    @Override
    public long position() {
        return this.position;
    }

    @Override
    public void seek(long pos) {
        this.position = pos;
    }

    public ElasticBuffer underlyingData() {
        return this.elasticBuffer;
    }
}

package net.viper.core.memory.internal;

import net.viper.ViperUnsafe;
import net.viper.core.memory.pooling.bytes.BytesMemoryRecycler;
import sun.misc.Unsafe;

import java.util.Arrays;

public class SegmentedBytes {
    private static final Unsafe unsafe = ViperUnsafe.unsafe();

    private byte[][] segmentBytes;
    private final int log2OfSegmentLength;
    private final int bitmask;
    private final BytesMemoryRecycler memoryRecycler;

    public SegmentedBytes(BytesMemoryRecycler recycler) {
        this.memoryRecycler = recycler;
        this.log2OfSegmentLength = recycler.log2OfBytesLength();
        this.bitmask = (1 << this.log2OfSegmentLength) - 1;
        this.segmentBytes = new byte[2][];
    }

    public void set(long pos, byte value) {
        int whichSeg = (int) (pos >>> this.log2OfSegmentLength);
        int whichByte = (int) (pos & this.bitmask);
        ensureCapacity(whichSeg);

        this.segmentBytes[whichSeg][whichByte] = value;
    }

    public byte get(long pos) {
        int whichSeg = (int) (pos >>> this.log2OfSegmentLength);
        int whichByte = (int) (pos & this.bitmask);
        ensureCapacity(whichSeg);

        return this.segmentBytes[whichSeg][whichByte];
    }

    public byte[] get(long pos, int len) {
        int segmentLength = (1 << this.log2OfSegmentLength);
        byte[] result = new byte[len];
        int resultWritingPosition = 0;

        while (len > 0) {
            int whichSeg = (int) (pos >>> this.log2OfSegmentLength);
            int whichByte = (int) (pos & this.bitmask);
            ensureCapacity(whichSeg);

            int lenToCopy = Math.min(len, segmentLength - whichByte);

            orderedCopy(this.segmentBytes[whichSeg], whichByte, result, resultWritingPosition, lenToCopy);

            len -= lenToCopy;
            pos += lenToCopy;
            resultWritingPosition += lenToCopy;
        }

        return result;
    }

    public void copy(SegmentedBytes src, long srcPos, long destPos, long len) {
        int segmentLength = 1 << this.log2OfSegmentLength;
        while (len > 0) {
            int srcWhichSeg = (int) (srcPos >>> this.log2OfSegmentLength);
            int srcWhichByte = (int) (srcPos & this.bitmask);

            int destWhichSeg = (int) (destPos >>> this.log2OfSegmentLength);
            int destWhichByte = (int) (destPos & this.bitmask);

            src.ensureCapacity(srcWhichSeg);
            ensureCapacity(destWhichSeg);

            int takeMinRemainingBytes = Math.min(segmentLength - destWhichByte, segmentLength - srcWhichByte);
            int lenToCopy = (int) Math.min(takeMinRemainingBytes, len);

            System.arraycopy(src.segmentBytes[srcWhichSeg],
                    srcWhichByte,
                    this.segmentBytes[destWhichSeg],
                    destWhichByte,
                    lenToCopy);

            len -= lenToCopy;
            srcPos += lenToCopy;
            destPos += lenToCopy;
        }
    }

    public void orderedCopy(SegmentedBytes src, long srcPos, long destPos, long len) {
        int segmentLength = 1 << this.log2OfSegmentLength;
        while (len > 0) {
            int srcWhichSeg = (int) (srcPos >>> this.log2OfSegmentLength);
            int srcWhichByte = (int) (srcPos & this.bitmask);
            int destWhichSeg = (int) (destPos >>> this.log2OfSegmentLength);
            int destWhichByte = (int) (destPos & this.bitmask);

            src.ensureCapacity(srcWhichSeg);
            ensureCapacity(destWhichSeg);

            int takeMinRemainingBytes = Math.min(segmentLength - destWhichByte, segmentLength - srcWhichByte);
            int lenToCopy = (int) Math.min(takeMinRemainingBytes, len);

            orderedCopy(src.segmentBytes[srcWhichSeg],
                    srcWhichByte,
                    this.segmentBytes[destWhichSeg],
                    destWhichByte,
                    lenToCopy);

            len -= lenToCopy;
            srcPos += lenToCopy;
            destPos += lenToCopy;
        }
    }

    public void orderedCopy(byte[] src, int srcPos, long destPos, int len) {
        int segmentLength = (1 << this.log2OfSegmentLength);
        while (len > 0) {
            int whichSeg = (int) (destPos >>> this.log2OfSegmentLength);
            int whichByte = (int) (destPos & this.bitmask);

            ensureCapacity(whichSeg);

            int lenToCopy = Math.min(len, segmentLength - whichByte);

            orderedCopy(src, srcPos, this.segmentBytes[whichSeg], whichByte, lenToCopy);

            len -= lenToCopy;
            srcPos += lenToCopy;
            destPos += lenToCopy;
        }
    }

    public boolean equalRange(long startPos, SegmentedBytes other, long otherStartPos, long len) {
        for (long i = 0; i < len; i++) {
            if (get(startPos + i) != other.get(otherStartPos + i))
                return false;
        }

        return true;
    }

    public long length() {
        long len = 0;

        for (byte[] seg : this.segmentBytes)
            if (seg != null)
                len += seg.length;

        return len;
    }

    private void orderedCopy(byte[] src, int srcPos, byte[] dest, int destPos, int len) {
        int srcEndPos = srcPos + len;
        destPos += Unsafe.ARRAY_BYTE_BASE_OFFSET;

        while (srcPos < srcEndPos)
            unsafe.putByteVolatile(dest, destPos++, src[srcPos++]);
    }

    private void ensureCapacity(int segIndex) {
        while (segIndex >= this.segmentBytes.length)
            this.segmentBytes = Arrays.copyOf(this.segmentBytes, this.segmentBytes.length * 3 / 2);

        if (this.segmentBytes[segIndex] == null)
            this.segmentBytes[segIndex] = this.memoryRecycler.bytes();
    }

    private void destroy() {
        for (byte[] seg : this.segmentBytes)
            if (seg != null)
                this.memoryRecycler.pool(seg);
    }
}

package net.viper.core.memory.internal;

import net.viper.core.memory.ElasticBuffer;
import net.viper.core.memory.pooling.buffers.BufferRecycler;
import net.viper.exceptions.ViperRuntimeException;
import net.viper.utils.logic.Maths;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class NativeElasticBuffer implements ElasticBuffer {
    private static final Logger LOG = Logger.getLogger(NativeElasticBuffer.class.getName());

    private final List<ByteBuffer> segmentBuffer = new ArrayList<>();
    private final BufferRecycler bufferRecycler;
    private final int log2OfBufCap;
    private final int bitmask;

    private boolean destroyed;

    public NativeElasticBuffer(BufferRecycler bufferRecycler) {
        this.bufferRecycler = bufferRecycler;
        this.log2OfBufCap = bufferRecycler.log2OfBufferCapacity();
        this.bitmask = (1 << this.log2OfBufCap) - 1;

        appendBuffer();
    }

    @Override
    public long capacity() {
        return this.segmentBuffer.stream()
                .mapToLong(Buffer::capacity)
                .sum();
    }

    @Override
    public void put(long pos, byte b) {
        checkNotDestroy();

        int whichBuf = (int) (pos >>> this.log2OfBufCap);
        int whichByte = (int) (pos & this.bitmask);
        ensureCapacity(whichBuf, whichByte);

        this.segmentBuffer.get(whichBuf).put(whichByte, b);
    }

    @Override
    public void put(long pos, byte[] bytes) {
        checkNotDestroy();

        int bytesLen = bytes.length;
        int readBytesOffset = 0;
        while (bytesLen > 0) {
            int whichBuf = (int) (pos >>> this.log2OfBufCap);
            int whichByte = (int) (pos & this.bitmask);
            ensureCapacity(whichBuf, whichByte);

            ByteBuffer buffer = this.segmentBuffer.get(whichBuf);
            int copiedBytes = Maths.min(bytesLen,
                    this.bufferRecycler.bufferChunkSize(),
                    (buffer.capacity() - whichByte));

            buffer.put(whichByte, bytes, readBytesOffset, copiedBytes);

            bytesLen -= copiedBytes;
            pos += copiedBytes;
            readBytesOffset += copiedBytes;
        }
    }

    @Override
    public byte get(long pos) {
        checkNotDestroy();

        int whichBuf = (int) (pos >>> this.log2OfBufCap);
        int whichByte = (int) (pos & this.bitmask);

        return this.segmentBuffer.get(whichBuf).get(whichByte);
    }

    @Override
    public byte[] get(long pos, int len) {
        checkNotDestroy();

        byte[] result = new byte[len];
        int writeResultOffset = 0;

        while (len > 0) {
            int whichBuf = (int) (pos >>> this.log2OfBufCap);
            int whichByte = (int)(pos & this.bitmask);

            ByteBuffer buffer = this.segmentBuffer.get(whichBuf);
            int copiedBytes = Maths.min(len,
                    this.bufferRecycler.bufferChunkSize(),
                    (buffer.capacity() - whichByte));

            buffer.get(whichByte, result, writeResultOffset, copiedBytes);

            len -= copiedBytes;
            pos += copiedBytes;
            writeResultOffset += copiedBytes;
        }

        return result;
    }

    @Override
    public void destroy() {
        for (ByteBuffer buffer : this.segmentBuffer)
            this.bufferRecycler.pool(buffer);

        this.segmentBuffer.clear();
        this.destroyed = true;
    }

    private void ensureCapacity(int segmentIndex, int byteIndex) {
        if (segmentIndex > this.segmentBuffer.size())
            throw new IllegalArgumentException("Invalid position lead to out of bound buffer index");

        // means position is over of last buffer length.
        if (byteIndex == 0 && segmentIndex == this.segmentBuffer.size()) {
            appendBuffer();
            return;
        }

        if (byteIndex >= this.segmentBuffer.get(segmentIndex).capacity()) {
            growBufferCapacity(segmentIndex, byteIndex);
        }
    }

    private void checkNotDestroy() {
        if (this.destroyed)
            throw new ViperRuntimeException("Access a destroyed ElasticBuffer");
    }

    private void growBufferCapacity(int segmentIndex, int byteIndex) {
        LOG.finest("Starting grow buffer capacity");
        long startTime = System.currentTimeMillis();

        ByteBuffer buffer = this.segmentBuffer.get(segmentIndex);
        int newCapacity = buffer.capacity();
        while (byteIndex >= newCapacity)
            newCapacity *= 2;

        ByteBuffer growingBuffer = ByteBuffer.allocateDirect(newCapacity);
        buffer.rewind();
        growingBuffer.put(buffer);
        growingBuffer.rewind();

        this.segmentBuffer.set(segmentIndex, growingBuffer);

        // TODO: do need to fill buffer with Zero
        buffer.clear();
        this.bufferRecycler.pool(buffer);

        long endTime = System.currentTimeMillis();
        LOG.finest(String.format("Grow buffer elapse_time=%s, new_cap=%s",
                (endTime - startTime), newCapacity));
    }

    private void appendBuffer() {
        ByteBuffer buffer = this.bufferRecycler.byteBuffer(this.bufferRecycler.bufferChunkSize());
        this.segmentBuffer.addLast(buffer);
        LOG.finest("Append new buffer to back-spine");
    }
}

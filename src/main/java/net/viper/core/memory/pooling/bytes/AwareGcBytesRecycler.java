package net.viper.core.memory.pooling.bytes;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryManagerMXBean;
import java.util.Arrays;
import java.util.List;

public class AwareGcBytesRecycler implements BytesMemoryRecycler {
    private static final List<String> LOW_PAUSE_GC = Arrays.asList("GPGC", "Shenandoah", "ZGC");

    private final BytesMemoryRecycler delegateInstance;

    public AwareGcBytesRecycler(int log2OfBytesLength) {
        boolean isLowGC = ManagementFactory.getGarbageCollectorMXBeans()
                .stream()
                .map(MemoryManagerMXBean::getName)
                .map(name -> name.split(" ")[0])
                .anyMatch(LOW_PAUSE_GC::contains);

        this.delegateInstance = isLowGC ? new OnDemandBytesRecycler(log2OfBytesLength)
                                        : new ReusableBytesRecycler(log2OfBytesLength);
    }

    @Override
    public int log2OfBytesLength() {
       return this.delegateInstance.log2OfBytesLength();
    }

    @Override
    public byte[] bytes() {
        return this.delegateInstance.bytes();
    }

    @Override
    public void pool(byte[] bytes) {
        this.delegateInstance.pool(bytes);
    }

    @Override
    public void advance() {
        this.delegateInstance.advance();
    }
}

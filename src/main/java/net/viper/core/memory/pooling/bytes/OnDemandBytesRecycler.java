package net.viper.core.memory.pooling.bytes;

public final class OnDemandBytesRecycler implements BytesMemoryRecycler {
    /**
     * Caution, should not use for runtime application, recommend use to test.
     */
    private static final int SMALL_LOG2_OF_BYTES_SEGMENT_SIZE = 8;

    private final int log2OfBytesLength;

    public static OnDemandBytesRecycler DEFAULT_INSTANCE = new OnDemandBytesRecycler(DEFAULT_LOG2_OF_BYTES_SEGMENT_SIZE);
    public static OnDemandBytesRecycler PORTION_INSTANCE = new OnDemandBytesRecycler(SMALL_LOG2_OF_BYTES_SEGMENT_SIZE);

    public OnDemandBytesRecycler(int log2OfBytesLength) {
        this.log2OfBytesLength = log2OfBytesLength;
    }

    @Override
    public int log2OfBytesLength() {
        return this.log2OfBytesLength;
    }

    @Override
    public byte[] bytes() {
        return new byte[(1 << this.log2OfBytesLength)];
    }

    @Override
    public void pool(byte[] bytes) {
        /* do nothing */
    }

    @Override
    public void advance() {
        /* do nothing */
    }
}

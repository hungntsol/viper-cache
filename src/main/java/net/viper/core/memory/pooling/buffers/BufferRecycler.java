package net.viper.core.memory.pooling.buffers;

import java.nio.ByteBuffer;

public interface BufferRecycler {
    int DEFAULT_LOG2_OF_BUFFER_SEGMENT_SIZE = 30;
    int SMALL_LOG2_OF_BUFFER_SEGMENT_SIZE = 10;

    int log2OfBufferCapacity();

    int bufferChunkSize();

    ByteBuffer byteBuffer(int cap);

    void pool(ByteBuffer instance);

    void advance();

    static void ensureBufferLengthLimit(int log2OfBufferLength) {
        if (log2OfBufferLength > 30)
            throw new IllegalArgumentException("Could not have BufferSize length > 2^30"); // ByteBuffer < 2Gb.
    }
}

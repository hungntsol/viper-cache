package net.viper.core.memory.pooling.bytes;

import java.util.ArrayDeque;
import java.util.Deque;

public final class ReusableBytesRecycler implements BytesMemoryRecycler {
    private final int log2OfBytesLength;
    private final Recyclable<byte[]> recyclable;

    public static ReusableBytesRecycler DEFAULT_INSTANCE =
            new ReusableBytesRecycler(DEFAULT_LOG2_OF_BYTES_SEGMENT_SIZE);

    public ReusableBytesRecycler(int log2ofBytesLength) {
        this.log2OfBytesLength = log2ofBytesLength;

        this.recyclable = new Recyclable<>(() ->
                new byte[(1 << log2ofBytesLength)]);
    }

    @Override
    public int log2OfBytesLength() {
        return this.log2OfBytesLength;
    }

    @Override
    public byte[] bytes() {
        return this.recyclable.get();
    }

    @Override
    public void pool(byte[] bytes) {
        this.recyclable.pool(bytes);
    }

    @Override
    public void advance() {
        this.recyclable.advance();
    }

    private static class Recyclable<T> {
        private final Creatable<T> creatable;
        private Deque<T> currentSegment;
        private Deque<T> nextSegment;

        Recyclable(Creatable<T> creatable) {
            this.creatable = creatable;
            this.currentSegment = new ArrayDeque<>();
            this.nextSegment = new ArrayDeque<>();
        }

        T get() {
            if (!currentSegment.isEmpty()) {
                return currentSegment.removeFirst();
            }

            return creatable.create();
        }

        void pool(T instance) {
            nextSegment.addLast(instance);
        }

        void advance() {
            if (nextSegment.size() > currentSegment.size()) {
                Deque<T> tmp = nextSegment;
                nextSegment = currentSegment;
                currentSegment = tmp;
            }

            currentSegment.addAll(nextSegment);
            nextSegment.clear();
        }
    }

    private interface Creatable<T> {
        T create();
    }
}

package net.viper.core.memory.pooling.bytes;

public interface BytesMemoryRecycler {
    int DEFAULT_LOG2_OF_BYTES_SEGMENT_SIZE = 16;




    int log2OfBytesLength();

    byte[] bytes();

    void pool(byte[] bytes);

    void advance();
}

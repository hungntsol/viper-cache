package net.viper.core.memory.pooling.buffers;

import java.nio.ByteBuffer;

public class OnDemandBufferRecycler implements BufferRecycler {
    private final int log2OfBufferCapacity;
    private final int bufferChunkSize;

    public OnDemandBufferRecycler(int log2OfBufferCapacity) {
        BufferRecycler.ensureBufferLengthLimit(log2OfBufferCapacity);

        this.log2OfBufferCapacity = log2OfBufferCapacity;

        if (log2OfBufferCapacity < 16)
            this.bufferChunkSize = 1 << log2OfBufferCapacity;
        else
            this.bufferChunkSize = 1 << 16;
    }

    public static OnDemandBufferRecycler DEFAULT_INSTANCE =
            new OnDemandBufferRecycler(DEFAULT_LOG2_OF_BUFFER_SEGMENT_SIZE);

    public static OnDemandBufferRecycler PORTION_INSTANCE =
            new OnDemandBufferRecycler(SMALL_LOG2_OF_BUFFER_SEGMENT_SIZE);

    @Override
    public int log2OfBufferCapacity() {
        return this.log2OfBufferCapacity;
    }

    @Override
    public int bufferChunkSize() {
        return this.bufferChunkSize;
    }

    @Override
    public ByteBuffer byteBuffer(int cap) {
        assert (cap & (cap - 1)) == 0;
        return ByteBuffer.allocateDirect(cap);
    }

    @Override
    public void pool(ByteBuffer instance) {
        // Do nothing
    }

    @Override
    public void advance() {
        // Do nothing
    }
}

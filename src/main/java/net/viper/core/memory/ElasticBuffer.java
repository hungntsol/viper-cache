package net.viper.core.memory;

public interface ElasticBuffer {
    long capacity();

    void put(long pos, byte b);

    void put(long pos, byte[] bytes);

    byte get(long pos);

    byte[] get(long pos, int len);

    void destroy();
}

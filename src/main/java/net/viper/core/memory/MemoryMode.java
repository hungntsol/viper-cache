package net.viper.core.memory;

public enum MemoryMode {
    ON_HEAP, // eager load into main memory JVM heap.
    VIRTUAL_MAPPED_MEMORY; // lazy load into main memory, off-heap

    public boolean isFilterSupport(MemoryMode mode) {
        return mode.equals(ON_HEAP);
    }
}

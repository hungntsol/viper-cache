package net.viper.core.memory;

public interface Bytes {
    void write(byte b);

    void write(byte[] buf);

    byte read();

    byte read(long pos);

    byte[] read(long pos, int len);

    long position();

    void seek(long pos);

    void clear();
}

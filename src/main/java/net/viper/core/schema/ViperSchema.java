package net.viper.core.schema;

import net.viper.core.objects.ViperBlobInput;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ViperSchema {
    private final String clazzName;

    public ViperSchema(String clazzName) {
        if (clazzName == null || clazzName.isEmpty())
            throw new IllegalArgumentException("ViperSchema name is not null or empty");

        this.clazzName = clazzName;
    }

    public static ViperSchema readFrom(ViperBlobInput input) throws IOException {
        String typeName = input.readUTF();
        return new ViperSchema(typeName);
    }

    public void write(OutputStream os) throws IOException {
        try (
                DataOutputStream dos = new DataOutputStream(os);
        ) {
            dos.writeUTF(this.clazzName);
        }

    }

    public final String getClazzName() {
        return this.clazzName;
    }

    public final boolean isDefaultExtractor() {
        return true; // TODO: @@@ we will pass extractor function here.
    }
}

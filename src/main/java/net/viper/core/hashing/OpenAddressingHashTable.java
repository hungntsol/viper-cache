package net.viper.core.hashing;

import java.util.Arrays;

@SuppressWarnings("unchecked")
public class OpenAddressingHashTable<V> {

    public static final float DEFAULT_LOAD_FACTOR_THRESHOLD = 0.75f;

    private int capacity;
    private int size;
    private Entry<V>[] table;
    private final float loadFactorThreshold;

    public OpenAddressingHashTable() {
        this(1024, DEFAULT_LOAD_FACTOR_THRESHOLD);
    }

    public OpenAddressingHashTable(int initialCap, float loadFactorThreshold) {
        this.capacity = initialCap;
        this.size = 0;
        this.table = new Entry[this.capacity];
        this.loadFactorThreshold = loadFactorThreshold;
    }

    public int capacity() {
        return this.capacity;
    }

    public int size() {
        return this.size;
    }

    public int put(String key, V value) {
        return put(key.getBytes(), value);
    }

    public int put(byte[] key, V value) {
        int hash = hash(key);
        this.table[hash] = new Entry<>(key, value);
        this.size++;

        if (this.size > loadFactor())
            resize();

        return hash;
    }

    public V remove(String key) {
        return remove(key.getBytes());
    }

    public V remove(byte[] key) {
        int hashIndex = hash(key);
        final Entry<V> entry = this.table[hashIndex];

        if (entry != null && !entry.isFree) {
            V result = entry.value;
            entry.free();
            this.size--;

            return result;
        }

        return null;
    }

    public boolean contains(String key) {
        return contains(key.getBytes());
    }

    public boolean contains(byte[] key) {
        int hash = hash(key);
        final Entry<V> entry = this.table[hash];

        return entry != null && !entry.isFree;
    }

    public V search(String key) {
        return search(key.getBytes());
    }

    public V search(byte[] key) {
        int hash = hash(key);
        final Entry<V> entry = this.table[hash];

        if (entry != null && !entry.isFree) {
            return entry.value;
        }

        return null;
    }

    public int hash(byte[] key) {
        int hash = hashCode(key);
        int index = forIndex(hash);

        while (true) {
            Entry<V> entry = this.table[index];
            if (entry == null || Arrays.equals(key, entry.key))
                return index;
            else if (entry.isFree)
                break;
            else
                index = linearProbing(index);
        }

        int xspot = index;

        while (this.table[index] != null) {
            index = linearProbing(index);

            if (index == xspot) {
                resize();
                index = forIndex(hash);
            }

            if (this.table[index] == null)
                return xspot;

            if (Arrays.equals(this.table[index].key, key))
                return index;
        }

        return xspot;
    }

    private int loadFactor() {
        return (int) (this.loadFactorThreshold * capacity());
    }

    private void resize() {
        Entry<V>[] tmpTable = this.table;
        this.capacity = capacity * 2;

        this.table = new Entry[this.capacity];
        this.size = 0;

        for (final Entry<V> entry : tmpTable) {
            if (entry != null && !entry.isFree)
                put(entry.key, entry.value);
        }
    }

    private int linearProbing(int index) {
        return (index + 1) % capacity();
    }

    private int forIndex(int hash) {
        return hash & (capacity() - 1);
    }

    private int hashCode(byte[] b) {
        return Murmur3Hash.hash(b);
    }

    static class Entry<V> {
        byte[] key;
        V value;
        boolean isFree;

        Entry(byte[] key, V value) {
            this.key = key;
            this.value = value;
            this.isFree = false;
        }

        void free() {
            this.isFree = true;
        }
    }
}

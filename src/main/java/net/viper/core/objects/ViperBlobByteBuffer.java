package net.viper.core.objects;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static java.nio.channels.FileChannel.MapMode.READ_ONLY;

public class ViperBlobByteBuffer {
    private final ByteBuffer[] backBuffers;   // array of MappedByteBuffers
    private final long capacity;        // in bytes
    private final int shift;
    private final int bitmask;

    private long position;              // within index 0 to capacity-1 in the underlying ByteBuffer

    public ViperBlobByteBuffer(ByteBuffer[] backBuffers, long capacity, int shift, int bitmask) {
        this(backBuffers, capacity, shift, bitmask, 0);
    }

    public ViperBlobByteBuffer(ByteBuffer[] backBuffers, long capacity, int shift, int bitmask, long position) {
        if (!backBuffers[0].order().equals(ByteOrder.BIG_ENDIAN))
            throw new UnsupportedOperationException("Only support BIG_ENDIAN Byte order");

        this.backBuffers = backBuffers;
        this.capacity = capacity;
        this.shift = shift;
        this.bitmask = bitmask;
        this.position = position;
    }

    public ViperBlobByteBuffer duplicate() {
        return new ViperBlobByteBuffer(this.backBuffers, this.capacity, this.shift, this.bitmask, this.position);
    }

    public static ViperBlobByteBuffer mmap(FileChannel channel, int singleBufferCapacity) throws IOException {
        long size = channel.size();
        if (size == 0) {
            throw new IllegalStateException("File to be mmap-ed has no data");
        }
        if ((singleBufferCapacity & (singleBufferCapacity - 1)) != 0) { // should be a power of 2
            throw new IllegalArgumentException("singleBufferCapacity must be a power of 2");
        }

        // divide into N buffers with an int capacity that is a power of 2
        final int bufferCapacity = size > (long) singleBufferCapacity
                                   ? singleBufferCapacity
                                   : Integer.highestOneBit((int) size);
        long bufferCount = size % bufferCapacity == 0
                           ? size / (long) bufferCapacity
                           : (size / (long) bufferCapacity) + 1;
        if (bufferCount > Integer.MAX_VALUE)
            throw new IllegalArgumentException("file too large; size=" + size);

        int shift = 31 - Integer.numberOfLeadingZeros(bufferCapacity); // log2
        int mask = (1 << shift) - 1;
        ByteBuffer[] spine = new MappedByteBuffer[(int) bufferCount];
        for (int i = 0; i < bufferCount; i++) {
            long pos = (long) i * bufferCapacity;
            int cap = i == (bufferCount - 1)
                      ? (int) (size - pos)
                      : bufferCapacity;
            ByteBuffer buffer = channel.map(READ_ONLY, pos, cap);
            spine[i] = buffer;
        }

        return new ViperBlobByteBuffer(spine, size, shift, mask);
    }

    public long position() {
        return this.position;
    }

    public ViperBlobByteBuffer position(long position) {
        if (position > this.capacity || position < 0)
            throw new IllegalArgumentException("invalid position; position=" + position + "; capacity=" + capacity);
        this.position = position;
        return this;
    }

    public byte getByte(long index) throws BufferUnderflowException {
        if (index < this.capacity) {
            int spineIndex = (int) (index >>> this.shift);
            int bufferIndex = (int) (index & this.bitmask);
            return this.backBuffers[spineIndex].get(bufferIndex);
        } else {
            // this situation occurs when read for bits near the end of the buffer requires reading a long value that
            // extends past the buffer capacity by upto Long.BYTES bytes. To handle this case,
            // return 0 for (index >= capacity - Long.BYTES && index < capacity )
            // these zero bytes will be discarded anyway when the returned long value is shifted to get the queried bits
            return (byte) 0;
        }
    }

    public long getLong(long startByteIndex) throws BufferUnderflowException {
        byte[] bytes = new byte[Long.BYTES];
        for (int i = 0; i < Long.BYTES; i++) {
            bytes[i] = getByte(startByteIndex + i);
        }

        return ((((long) (bytes[7])) << 56) |
                (((long) (bytes[6] & 0xff)) << 48) |
                (((long) (bytes[5] & 0xff)) << 40) |
                (((long) (bytes[4] & 0xff)) << 32) |
                (((long) (bytes[3] & 0xff)) << 24) |
                (((long) (bytes[2] & 0xff)) << 16) |
                (((long) (bytes[1] & 0xff)) << 8) |
                (((long) (bytes[0] & 0xff))));
    }
}

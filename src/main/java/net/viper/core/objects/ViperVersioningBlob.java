package net.viper.core.objects;

import net.viper.ViperConstants;

public abstract class ViperVersioningBlob {
    protected final long fromVersion;
    protected final long toVersion;

    protected final BlobType blobType;

    protected ViperVersioningBlob(long toVersion) {
        this(ViperConstants.VERSION_NULL, toVersion);
    }

    protected ViperVersioningBlob(long fromVersion, long toVersion) {
        assert fromVersion != toVersion;

        this.fromVersion = fromVersion;
        this.toVersion = toVersion;

        if (isDelta())
            this.blobType = BlobType.DELTA_JOURNAL;
        else
            this.blobType = BlobType.REVERSED_DELTA_JOURNAL;
    }

    public final boolean isDelta() {
        return this.toVersion > this.fromVersion;
    }

    public final boolean isReversedDelta() {
        return !isDelta();
    }

    public final BlobType getBlobType() {
        return blobType;
    }

    public final long getToVersion() {
        return toVersion;
    }

    public final long getFromVersion() {
        return fromVersion;
    }
}

package net.viper.core.objects;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface ViperStreamBlob {
    InputStream inputStream() throws IOException;

    default File file() throws IOException {
        throw new UnsupportedOperationException("default access file for ViperStreamBlob implementation");
    }
}

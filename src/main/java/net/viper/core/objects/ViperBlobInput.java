package net.viper.core.objects;

import net.viper.Jvm;
import net.viper.core.memory.MemoryMode;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Arrays;

public final class ViperBlobInput implements Closeable {
    public static final String UNKNOWN_BLOB_INPUT_TYPE_MSG = "Unknown BlobInput type";

    private final MemoryMode memoryMode;
    private Object input;
    private ViperBlobByteBuffer buffer;

    private ViperBlobInput(MemoryMode memoryMode) {
        this.memoryMode = memoryMode;
    }

    public static ViperBlobInput randomAccess(File file) throws IOException {
        return randomAccess(file, Jvm.MAX_SINGLE_BUFFER_BYTE_CAPACITY);
    }

    public static ViperBlobInput randomAccess(File f, int bufferCap) throws IOException {
        ViperBlobInput blobInput = new ViperBlobInput(MemoryMode.VIRTUAL_MAPPED_MEMORY);
        RandomAccessFile raf = new RandomAccessFile(f, Jvm.FILE_MODE_READ_ONLY);
        blobInput.input = raf;

        FileChannel fileChannel = raf.getChannel();
        blobInput.buffer = ViperBlobByteBuffer.mmap(fileChannel, bufferCap);

        return blobInput;
    }

    public static ViperBlobInput serial(byte[] bytes) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        return serial(bis);
    }

    public static ViperBlobInput serial(InputStream is) {
        ViperBlobInput blobInput = new ViperBlobInput(MemoryMode.ON_HEAP);
        blobInput.input = new DataInputStream(is);

        return blobInput;
    }

    /**
     * Closes this stream and releases any system resources associated
     * with it. If the stream is already closed then invoking this
     * method has no effect.
     *
     * <p> As noted in {@link AutoCloseable#close()}, cases where the
     * close may fail require careful attention. It is strongly advised
     * to relinquish the underlying resources and to internally
     * <em>mark</em> the {@code Closeable} as closed, prior to throwing
     * the {@code IOException}.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        if (input instanceof RandomAccessFile) {
            ((RandomAccessFile) input).close();
        } else if (input instanceof DataInputStream) {
            ((DataInputStream) input).close();
        } else {
            throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
        }
    }

    public MemoryMode getMemoryMode() {
        return memoryMode;
    }

    /**
     * @return Read next byte as int (range in 0-255 0x00-0x0ff), return -1 if end of input
     */
    public int read() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).read();
        } else if (isStream()) {
            return ((DataInputStream) input).read();
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public int read(byte[] b, int offset, int len) throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).read(b, offset, len);
        } else if (isStream()) {
            return ((DataInputStream) input).read(b, offset, len);
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public int readNBytes(byte[] bytes, int len) throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).read(bytes);
        } else if (isStream()) {
            byte[] readBytes = ((DataInputStream) input).readNBytes(len);
            System.arraycopy(readBytes, 0, bytes, 0, len);
            return bytes.length;
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public void seek(long pos) throws IOException {
        if (isFile()) {
            ((RandomAccessFile) input).seek(pos);
        } else if (isStream()) {
            throw new UnsupportedOperationException("Can not seek on BlobInput of type DataInputStream");
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public long getFilePointer() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).getFilePointer();
        } else if (isStream()) {
            throw new UnsupportedOperationException("Can not get file pointer for BlobInput of type " +
                    "DataInputStream");
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public short readShort() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).readShort();
        } else if (isStream()) {
            return ((DataInputStream) input).readShort();
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public int readInt() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).readInt();
        } else if (isStream()) {
            return ((DataInputStream) input).readInt();
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public long readLong() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).readLong();
        } else if (isStream()) {
            return ((DataInputStream) input).readLong();
        } else {
            throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
        }
    }

    public String readUTF() throws IOException {
        if (isFile()) {
            return ((RandomAccessFile) input).readUTF();
        } else if (isStream()) {
            return ((DataInputStream) input).readUTF();
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public long skipBytes(long n) throws IOException {
        if (isFile()) {
            long total = 0;
            int expected = 0;
            int actual = 0;
            do {
                expected = (n - total) > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) (n - total);
                actual = ((RandomAccessFile) input).skipBytes(expected);    // RandomAccessFile::skipBytes supports int
                total = total + actual;
            } while (total < n && actual > 0);
            return total;
        } else if (isStream()) {
            return ((DataInputStream) input).skip(n); // InputStream::skip supports long
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    public Object getInput() {
        return this.input;
    }

    public ViperBlobByteBuffer getBuffer() {
        if (isFile()) {
            return this.buffer;
        } else if (isStream()) {
            throw new UnsupportedOperationException("A stream can not have buffer");
        }

        throw new UnsupportedOperationException(UNKNOWN_BLOB_INPUT_TYPE_MSG);
    }

    private boolean isFile() {
        return this.input instanceof RandomAccessFile;
    }

    private boolean isStream() {
        return this.input instanceof DataInputStream;
    }
}

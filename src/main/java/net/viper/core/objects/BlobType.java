package net.viper.core.objects;

public enum BlobType {
    DELTA_JOURNAL("delta"),
    REVERSED_DELTA_JOURNAL("reversed_delta");

    private final String prefix;

    BlobType(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return this.prefix;
    }
}

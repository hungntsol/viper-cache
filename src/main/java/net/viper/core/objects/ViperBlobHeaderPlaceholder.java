package net.viper.core.objects;

import net.viper.core.schema.ViperSchema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ViperBlobHeaderPlaceholder {

    public static final int BLOB_HEADER_VERSION_ID = 2000;

    private long originRandomizedTag;
    private long destinationRandomizedTag;
    private Collection<ViperSchema> managedSchemas = new ArrayList<ViperSchema>();
    private Map<Integer, Class<?>> managedRegisteredResolverClasses = new HashMap<Integer, Class<?>>();

    public ViperBlobHeaderPlaceholder() {
    }

    public long getOriginRandomizedTag() {
        return this.originRandomizedTag;
    }

    public long getDestinationRandomizedTag() {
        return this.destinationRandomizedTag;
    }

    public void setOriginRandomizedTag(long originRandomizedTag) {
        this.originRandomizedTag = originRandomizedTag;
    }

    public void setDestinationRandomizedTag(long destinationRandomizedTag) {
        this.destinationRandomizedTag = destinationRandomizedTag;
    }

    public Collection<ViperSchema> getManagedSchemas() {
        return this.managedSchemas;
    }

    public void setManagedSchemas(Collection<ViperSchema> managedSchemas) {
        this.managedSchemas = managedSchemas;
    }

    public Map<Integer, Class<?>> getManagedRegisteredResolverClasses() {
        return managedRegisteredResolverClasses;
    }

    public void setManagedRegisteredResolverClasses(Map<Integer, Class<?>> managedRegisteredResolverClasses) {
        this.managedRegisteredResolverClasses = managedRegisteredResolverClasses;
    }
}
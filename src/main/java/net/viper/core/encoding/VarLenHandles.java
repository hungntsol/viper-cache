package net.viper.core.encoding;

import net.viper.core.memory.Bytes;
import net.viper.core.memory.store.NativeBytesStore;
import net.viper.core.objects.ViperBlobInput;
import net.viper.exceptions.ViperRuntimeException;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressWarnings("DuplicatedCode")
public class VarLenHandles {
    private static final byte NULL_BYTE = (byte) 0x80;
    private static final byte MASKING_BYTE = (byte) 0x7f;

    public static int writeNull(byte[] data, int pos) {
        data[pos++] = NULL_BYTE;
        return pos;
    }

    public static void writeNull(NativeBytesStore data) {
        data.write(NULL_BYTE);
    }

    public static int writeVarInt(byte[] data, int pos, int value) {
        if (value > 0xfffffff || value < 0) data[pos++] = (byte) (NULL_BYTE | (value >>> 28));
        if (value > 0x1fffff || value < 0) data[pos++] = (byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE));
        if (value > 0x3fff || value < 0) data[pos++] = (byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE));
        if (value > 0x7f || value < 0) data[pos++] = (byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE));
        data[pos++] = (byte) (value & MASKING_BYTE);

        return pos;
    }

    public static void writeVarInt(Bytes data, int value) {
        if (value > 0xfffffff || value < 0) data.write((byte) (NULL_BYTE | (value >>> 28)));
        if (value > 0x1fffff || value < 0) data.write((byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE)));
        if (value > 0x3fff || value < 0) data.write((byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE)));
        if (value > 0x7f || value < 0) data.write((byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE)));
        data.write((byte) (value & MASKING_BYTE));
    }

    public static void writeVarInt(OutputStream os, int value) throws IOException {
        if (value > 0xfffffff || value < 0) os.write((byte) (NULL_BYTE | (value >>> 28)));
        if (value > 0x1fffff || value < 0) os.write((byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE)));
        if (value > 0x3fff || value < 0) os.write((byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE)));
        if (value > 0x7f || value < 0) os.write((byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE)));
        os.write((byte) (value & MASKING_BYTE));
    }

    public static int writeVarLong(byte[] data, int pos, long value) {
        if (value < 0) data[pos++] = (byte) 0x81;
        if (value > 0xffffffffffffffL || value < 0)
            data[pos++] = ((byte) (NULL_BYTE | ((value >>> 56) & MASKING_BYTE)));
        if (value > 0x1ffffffffffffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 49) & MASKING_BYTE)));
        if (value > 0x3ffffffffffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 42) & MASKING_BYTE)));
        if (value > 0x7ffffffffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 35) & MASKING_BYTE)));
        if (value > 0xfffffffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 28) & MASKING_BYTE)));
        if (value > 0x1fffffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE)));
        if (value > 0x3fffL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE)));
        if (value > 0x7fL || value < 0) data[pos++] = ((byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE)));
        data[pos] = (byte) (value & MASKING_BYTE);

        return pos;
    }

    public static void writeVarLong(Bytes data, long value) {
        if (value < 0) data.write((byte) 0x81);
        if (value > 0xffffffffffffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 56) & MASKING_BYTE))));
        if (value > 0x1ffffffffffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 49) & MASKING_BYTE))));
        if (value > 0x3ffffffffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 42) & MASKING_BYTE))));
        if (value > 0x7ffffffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 35) & MASKING_BYTE))));
        if (value > 0xfffffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 28) & MASKING_BYTE))));
        if (value > 0x1fffffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE))));
        if (value > 0x3fffL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE))));
        if (value > 0x7fL || value < 0) data.write(((byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE))));
        data.write((byte) (value & MASKING_BYTE));
    }

    public static void writeVarLong(OutputStream os, long value) throws IOException {
        if (value < 0) os.write((byte) 0x81);
        if (value > 0xffffffffffffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 56) & MASKING_BYTE))));
        if (value > 0x1ffffffffffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 49) & MASKING_BYTE))));
        if (value > 0x3ffffffffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 42) & MASKING_BYTE))));
        if (value > 0x7ffffffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 35) & MASKING_BYTE))));
        if (value > 0xfffffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 28) & MASKING_BYTE))));
        if (value > 0x1fffffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 21) & MASKING_BYTE))));
        if (value > 0x3fffL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 14) & MASKING_BYTE))));
        if (value > 0x7fL || value < 0) os.write(((byte) (NULL_BYTE | ((value >>> 7) & MASKING_BYTE))));
        os.write((byte) (value & MASKING_BYTE));
    }

    public static boolean readVarNull(byte[] data, int pos) {
        return data[pos] == NULL_BYTE;
    }

    public static boolean readVarNull(NativeBytesStore data, long pos) {
        return data.read(pos) == NULL_BYTE;
    }

    public static int readVarInt(byte[] data, int pos) {
        byte b = data[pos++];

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null value as Int");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = data[pos++];
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static int readVarInt(Bytes data, long pos) {
        byte b = data.read(pos++);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null value as Int");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = data.read(pos++);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static int readVarInt(InputStream is) throws IOException {
        byte b = readByteSafe(is);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null from stream as int");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = readByteSafe(is);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static int readVarInt(ViperBlobInput input) throws IOException {
        byte b = readByteSafe(input);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null from input as int");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = readByteSafe(input);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static long readVarLong(byte[] data, int pos) {
        byte b = data[pos++];

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null value as Long");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = data[pos++];
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static long readVarLong(Bytes data, int pos) {
        byte b = data.read(pos++);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null value as Long");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = data.read(pos++);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static long readVarLong(InputStream is) throws IOException {
        byte b = readByteSafe(is);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null from stream as long");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = readByteSafe(is);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static long readVarLong(ViperBlobInput input) throws IOException {
        byte b = readByteSafe(input);

        if (b == NULL_BYTE)
            throw new ViperRuntimeException("Attempt to read null from input as long");

        int value = b & MASKING_BYTE;
        while ((b & NULL_BYTE) != 0) {
            b = readByteSafe(input);
            value <<= 7;
            value |= (b & MASKING_BYTE);
        }

        return value;
    }

    public static int sizeOfVarInt(int value) {
        if (value < 0)
            return 5;
        if (value < 0x80)
            return 1;
        if (value < 0x4000)
            return 2;
        if (value < 0x200000)
            return 3;
        if (value < 0x10000000)
            return 4;
        return 5;
    }

    public static int sizeOfVarLong(long value) {
        if (value < 0L)
            return 10;
        if (value < 0x80L)
            return 1;
        if (value < 0x4000L)
            return 2;
        if (value < 0x200000L)
            return 3;
        if (value < 0x10000000L)
            return 4;
        if (value < 0x800000000L)
            return 5;
        if (value < 0x40000000000L)
            return 6;
        if (value < 0x2000000000000L)
            return 7;
        if (value < 0x100000000000000L)
            return 8;
        return 9;
    }

    private static byte readByteSafe(InputStream is) throws IOException {
        int i = is.read();

        if (i == -1)
            throw new EOFException("Unexpected end of stream");

        return (byte) i;
    }

    private static byte readByteSafe(ViperBlobInput input) throws IOException {
        int i = input.read();

        if (i == -1)
            throw new EOFException("Unexpected end of blob input");

        return (byte) i;
    }
}

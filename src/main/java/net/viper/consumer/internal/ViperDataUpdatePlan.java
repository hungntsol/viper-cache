package net.viper.consumer.internal;

import net.viper.ViperConstants;
import net.viper.consumer.read.ViperTransition;

import java.util.*;

public class ViperDataUpdatePlan implements Iterable<ViperTransition> {

    private final List<ViperTransition> transitions = new ArrayList<>();

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<ViperTransition> iterator() {
        return this.transitions.iterator();
    }

    public List<ViperTransition> getTransitions() {
        return this.transitions;
    }

    public ViperTransition getTransition(int index) {
        return this.transitions.get(index);
    }

    public int transitionSize() {
        return this.transitions.size();
    }

    public long getDestinationVersion() {
        return this.transitions.isEmpty() ? ViperConstants.VERSION_NULL : this.transitions.getLast().getVersion();
    }

    public long getDestinationVersion(long fromVersion) {
        long result = getDestinationVersion();
        return result == ViperConstants.VERSION_NULL ? fromVersion : result;
    }

    public void add(ViperTransition transition) {
        this.transitions.add(transition);
    }

    public void add(ViperDataUpdatePlan plan) {
        this.transitions.addAll(plan.getTransitions());
    }
}

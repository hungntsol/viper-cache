package net.viper.consumer.internal;

import net.viper.ViperConstants;
import net.viper.consumer.ViperConsumer;
import net.viper.consumer.read.ViperTransition;

public class ViperDataUpdater {

    private final ViperConsumer.BlobFetcher fetcher;

    public ViperDataUpdater(ViperConsumer.BlobFetcher fetcher) {
        this.fetcher = fetcher;
    }

    public ViperDataUpdatePlan plan(long pendingVersion, long destinationVersion) {
        if (pendingVersion == destinationVersion)
            return new ViperDataUpdatePlan();

        return goDeltaPlan(pendingVersion, destinationVersion);
    }

    private ViperDataUpdatePlan goDeltaPlan(long pendingVersion, long destinationVersion) {
        ViperDataUpdatePlan plan = new ViperDataUpdatePlan();

        if (pendingVersion < destinationVersion)
            applyDeltaPlan(pendingVersion, destinationVersion, plan);
        else if (pendingVersion > destinationVersion)
            applyReversedDeltaPlan(pendingVersion, destinationVersion, plan);

        return plan;
    }

    private long applyDeltaPlan(long pendingVersion, long destinationVersion, ViperDataUpdatePlan plan) {
        while (pendingVersion < destinationVersion)
            pendingVersion = includeDelta(plan, pendingVersion, destinationVersion);

        return pendingVersion;
    }

    private long applyReversedDeltaPlan(long pendingVersion, long destinationVersion, ViperDataUpdatePlan plan) {
        long currentVersion = pendingVersion;

        while (pendingVersion > destinationVersion) {
            pendingVersion = includeReversedDelta(plan, pendingVersion);
            if (pendingVersion != ViperConstants.VERSION_NULL)
                currentVersion = pendingVersion;
        }

        return currentVersion;
    }

    private long includeDelta(ViperDataUpdatePlan plan, long currentVersion, long destinationVersion) {
        ViperConsumer.HeaderBlob headerBlob = this.fetcher.fetchHeaderBlob(currentVersion + 1);
        ViperConsumer.Blob deltaBlob = this.fetcher.fetchDeltaBlob(currentVersion);
        ViperTransition transition = new ViperTransition(currentVersion,
                headerBlob, deltaBlob);

        if (deltaBlob == null)
            return ViperConstants.VERSION_LAST;

        if (deltaBlob.getToVersion() <= destinationVersion)
            plan.add(transition);

        return deltaBlob.getToVersion();
    }

    private long includeReversedDelta(ViperDataUpdatePlan plan, long currentVersion) {
        ViperConsumer.HeaderBlob headerBlob = this.fetcher.fetchHeaderBlob(currentVersion);
        ViperConsumer.Blob reversedDeltaBlob = this.fetcher.fetchReversedBlob(currentVersion);
        ViperTransition transition = new ViperTransition(currentVersion,
                headerBlob, reversedDeltaBlob);

        if (reversedDeltaBlob == null)
            return ViperConstants.VERSION_NULL;

        plan.add(transition);
        return reversedDeltaBlob.getToVersion();
    }
}

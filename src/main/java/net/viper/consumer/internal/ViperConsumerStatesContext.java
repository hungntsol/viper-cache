package net.viper.consumer.internal;

import com.esotericsoftware.kryo.Kryo;
import net.viper.consumer.read.ViperSchemaStateRead;
import net.viper.consumer.read.ViperSchemaStateReadImpl;
import net.viper.core.memory.pooling.buffers.BufferRecycler;
import net.viper.core.schema.ViperSchema;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ViperConsumerStatesContext {
    private final Kryo kryoInstance;
    private final Map<String, ViperSchemaStateRead> typeToStateReadMap;
    private final BufferRecycler bufferRecycler;

    public ViperConsumerStatesContext(Kryo kryoInstance, BufferRecycler bufferRecycler) {
        this.kryoInstance = kryoInstance;
        this.bufferRecycler = bufferRecycler;
        this.typeToStateReadMap = new ConcurrentHashMap<>();
    }

    public void registerClassResolver(Map<Integer, Class<?>> registrations) {
        for (Map.Entry<Integer, Class<?>> registration : registrations.entrySet()) {
            // notice: keep in mind that, kryo will auto check a registration is existed.
            this.kryoInstance.register(registration.getValue(), registration.getKey());
        }
    }

    public void registerSchemaModel(Collection<ViperSchema> schemas) {
        for (ViperSchema schema : schemas) {
            if (this.typeToStateReadMap.containsKey(schema.getClazzName())) {
                continue;
            }

            ViperSchemaStateRead schemaStateRead = new ViperSchemaStateReadImpl(schema, this.bufferRecycler);
            this.typeToStateReadMap.put(schema.getClazzName(), schemaStateRead);
        }
    }

    public ViperSchemaStateRead getSchemaState(String typeName) {
        return this.typeToStateReadMap.get(typeName);
    }
}

package net.viper.consumer.internal;

import net.viper.ViperConstants;
import net.viper.consumer.ViperConsumer;
import net.viper.consumer.read.ViperBlobReaderImpl;
import net.viper.consumer.read.ViperStatesReadEngine;
import net.viper.core.memory.MemoryMode;

import java.util.logging.Logger;

public class ViperConsumerController {
    private static final Logger LOG = Logger.getLogger(ViperConsumerController.class.getName());

    private volatile ViperDataStorage dataStorage;
    private final ViperDataUpdater dataUpdater;
    private final MemoryMode memoryMode;
    private final ViperStatesReadEngine statesReadEngine;

    public ViperConsumerController(
            ViperDataUpdater dataUpdater,
            MemoryMode memoryMode,
            ViperStatesReadEngine statesReadEngine) {
        this.dataUpdater = dataUpdater;
        this.memoryMode = memoryMode;
        this.statesReadEngine = statesReadEngine;

        this.dataStorage = new ViperDataStorage(
                new ViperTransitionStats(),
                this.memoryMode,
                new ViperBlobReaderImpl(this.memoryMode, this.statesReadEngine));
    }

    // TODO: @@@ entry point for consumer
    public synchronized boolean update(ViperConsumer.VersionInformation versionInformation) throws Throwable {
        long requestVersion = versionInformation.version();
        if (requestVersion == currentVersion()) {
            LOG.warning("No version to update");
            if (requestVersion == ViperConstants.VERSION_NULL && this.dataStorage == null) {
                this.dataStorage = new ViperDataStorage(
                        new ViperTransitionStats(),
                        this.memoryMode,
                        new ViperBlobReaderImpl(this.memoryMode, this.statesReadEngine));
            }

            return true;
        }

        ViperDataUpdatePlan updatePlan = this.dataUpdater.plan(currentVersion(), requestVersion);

        if (updatePlan.transitionSize() == 0 && requestVersion == ViperConstants.VERSION_LAST)
            throw new IllegalArgumentException("Could not create an update-plan, no existed version for " +
                    "request_version = " + requestVersion);

        if (updatePlan.getDestinationVersion(requestVersion) == currentVersion())
            return true;

        this.dataStorage.update(updatePlan);

        return currentVersion() == requestVersion;
    }

    public long currentVersion() {
        return this.dataStorage == null ? ViperConstants.VERSION_NULL : this.dataStorage.currentVersion();
    }
}

package net.viper.consumer.internal;

import net.viper.consumer.ViperConsumer;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class ViperAbstractConsumer implements ViperConsumer {
    protected final ReadWriteLock fetchLock;

    protected ViperAbstractConsumer() {
        this.fetchLock = new ReentrantReadWriteLock();
    }

    protected void runFetchVersion(long version) {
        throw new UnsupportedOperationException("not implemented");
    }
}

package net.viper.consumer.internal;

import net.viper.ViperConstants;
import net.viper.consumer.ViperConsumer;
import net.viper.consumer.read.ViperBlobReader;
import net.viper.consumer.read.ViperTransition;
import net.viper.core.memory.MemoryMode;
import net.viper.core.objects.ViperBlobInput;

import java.io.IOException;
import java.util.logging.Logger;


public final class ViperDataStorage {
    private static final Logger LOG = Logger.getLogger(ViperDataStorage.class.getName());

    private final ViperTransitionStats transitionStats;
    private final MemoryMode memoryMode;
    private final ViperBlobReader blobReader;

    private long currentVersion = ViperConstants.VERSION_NULL;

    public ViperDataStorage(ViperTransitionStats transitionStats, MemoryMode memoryMode, ViperBlobReader blobReader) {
        this.transitionStats = transitionStats;
        this.memoryMode = memoryMode;
        this.blobReader = blobReader;
    }

    public long currentVersion() {
        return this.currentVersion;
    }

    public void currentVersion(long version) {
        this.currentVersion = version;
    }

    public void update(ViperDataUpdatePlan plan) throws IOException {
        applyDeltaPlan(plan);
    }

    private void applyDeltaPlan(ViperDataUpdatePlan plan) throws IOException {
        for (ViperTransition transition : plan.getTransitions()) {
            applyTransition(transition);
        }
    }

    private void applyTransition(ViperTransition transition) throws IOException {
        if (!this.memoryMode.equals(MemoryMode.ON_HEAP)) {
            LOG.warning("Skipping, apply delta-transition must be in ON_HEAP memory mode");
            return;
        }

        applyHeader(transition.getHeaderBlob());
        applyStateEngineTransition(transition.getDeltaBlob());
    }

    private void applyHeader(ViperConsumer.HeaderBlob headerBlob) throws IOException {
        this.blobReader.applyHeader(headerBlob.inputStream());
    }

    private void applyStateEngineTransition(ViperConsumer.Blob deltaBlob) throws IOException {

        try (ViperBlobInput input = ViperBlobInput.serial(deltaBlob.inputStream())) {
            this.blobReader.applyDelta(input);
        } catch (Throwable cause) {
            this.transitionStats.markFailTransition(deltaBlob);
            throw cause;
        }


        currentVersion(deltaBlob.getToVersion());

        LOG.info(String.format("[DELTA-TRANSITION] transition complete; from_v = %d; to_v = %d",
                deltaBlob.getFromVersion(), deltaBlob.getToVersion()));
    }
}

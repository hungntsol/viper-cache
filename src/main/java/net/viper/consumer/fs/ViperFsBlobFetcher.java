package net.viper.consumer.fs;

import net.viper.ViperConstants;
import net.viper.consumer.ViperConsumer;
import net.viper.core.objects.BlobType;
import net.viper.exceptions.ViperRuntimeException;
import net.viper.utils.logic.IOs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

public class ViperFsBlobFetcher implements ViperConsumer.BlobFetcher {

    private static final Logger LOG = Logger.getLogger(ViperFsBlobFetcher.class.getName());

    private final Path blobStorePath;

    public ViperFsBlobFetcher(Path blobStorePath) {
        this.blobStorePath = blobStorePath;
        ensurePathDirExists(blobStorePath);
    }

    @Override
    public ViperConsumer.HeaderBlob fetchHeaderBlob(long version) {
        Path exectPath = this.blobStorePath.resolve("header-" + version);
        if (Files.exists(exectPath)) {
            return new ViperFsHeaderBlob(exectPath, version);
        }

        long maxVersionBeforeDesired = ViperConstants.VERSION_NULL;
        try (final DirectoryStream<Path> dirStream = Files.newDirectoryStream(this.blobStorePath)) {
            for (final Path path : dirStream) {
                String fileName = path.getFileName().toString();
                if (fileName.startsWith("header-" + version)) {
                    long parsedVersion = Long.parseLong(fileName.substring(fileName.lastIndexOf("-") + 1));
                    if (parsedVersion < version && parsedVersion > maxVersionBeforeDesired) {
                        maxVersionBeforeDesired = parsedVersion;
                    }
                }
            }
        } catch (IOException e) {
            LOG.severe("Error in fetch header blob; path = " + exectPath);
            throw new ViperRuntimeException(e);
        }

        return new ViperFsHeaderBlob(exectPath, version);
    }

    @Override
    public ViperConsumer.Blob fetchDeltaBlob(long version) {
        try (
                final DirectoryStream<Path> dirStream = Files.newDirectoryStream(this.blobStorePath)
        ) {
            for (final Path path : dirStream) {
                String filename = path.getFileName().toString();
                if (filename.startsWith(BlobType.DELTA_JOURNAL.getPrefix() + "-" + version)) {
                    long toVersion = Long.parseLong(filename.substring(filename.lastIndexOf("-") + 1));
                    return fsBlob(BlobType.DELTA_JOURNAL, version, toVersion);
                }
            }
        } catch (IOException e) {
            LOG.severe("Error in fetch delta blob; version = " + version);
            throw new ViperRuntimeException(e);
        }

        return null;
    }

    @Override
    public ViperConsumer.Blob fetchReversedBlob(long version) {
        try (
                final DirectoryStream<Path> dirStream = Files.newDirectoryStream(this.blobStorePath)
        ) {
            for (final Path path : dirStream) {
                String filename = path.getFileName().toString();
                if (filename.startsWith(BlobType.REVERSED_DELTA_JOURNAL.getPrefix() + "-" + version)) {
                    long toVersion = Long.parseLong(filename.substring(filename.lastIndexOf("-") + 1));
                    return fsBlob(BlobType.REVERSED_DELTA_JOURNAL, version, toVersion);
                }
            }
        } catch (IOException e) {
            LOG.severe("Error in fetch reversed_delta blob; version = " + version);
            throw new ViperRuntimeException(e);
        }

        return null;
    }

    private void ensurePathDirExists(Path path) {
        IOs.mkdirs(path);
    }

    private ViperConsumer.Blob fsBlob(BlobType blobType, long currentVersion, long destinationVersion) {
        Path path;
        return switch (blobType) {
            case DELTA_JOURNAL, REVERSED_DELTA_JOURNAL -> {
                path = this.blobStorePath.resolve(String.format("%s-%d-%d", blobType.getPrefix(), currentVersion,
                        destinationVersion));
                yield new ViperFsBlob(currentVersion, destinationVersion, path);
            }
        };
    }

    private static class ViperFsHeaderBlob extends ViperConsumer.HeaderBlob {

        private final Path path;

        private ViperFsHeaderBlob(Path path, long version) {
            super(version);
            this.path = path;
        }

        @Override
        public InputStream inputStream() throws IOException {
            return new BufferedInputStream(Files.newInputStream(this.path));
        }

        @Override
        public File file() throws IOException {
            return this.path.toFile();
        }
    }

    private static class ViperFsBlob extends ViperConsumer.Blob {

        private final Path path;

        protected ViperFsBlob(long fromVersion, long toVersion, Path path) {
            super(fromVersion, toVersion);
            this.path = path;
        }

        @Override
        public InputStream inputStream() throws IOException {
            return new BufferedInputStream(Files.newInputStream(this.path));
        }

        @Override
        public File file() throws IOException {
            return this.path.toFile();
        }
    }
}

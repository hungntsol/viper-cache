package net.viper.consumer;

import net.viper.core.objects.ViperVersioningBlob;
import net.viper.core.objects.ViperStreamBlob;

public interface ViperConsumer {
    void fetch();

    interface BlobFetcher {
        HeaderBlob fetchHeaderBlob(long version);

        Blob fetchDeltaBlob(long version);

        Blob fetchReversedBlob(long version);
    }

    abstract class HeaderBlob implements ViperStreamBlob {

        protected final long version;

        protected HeaderBlob(long version) {
            this.version = version;
        }

        public final long getVersion() {
            return this.version;
        }
    }

    abstract class Blob extends ViperVersioningBlob implements ViperStreamBlob {

        protected Blob(long toVersion) {
            super(toVersion);
        }

        protected Blob(long fromVersion, long toVersion) {
            super(fromVersion, toVersion);
        }
    }

    record VersionInformation(long version) {}
}

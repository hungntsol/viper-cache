package net.viper.consumer.read;

import net.viper.consumer.ViperConsumer;

public class ViperTransition {
    private long version;
    private ViperConsumer.HeaderBlob headerBlob;
    private ViperConsumer.Blob deltaBlob;

    public ViperTransition(long version, ViperConsumer.HeaderBlob headerBlob, ViperConsumer.Blob deltaBlob) {
        this.version = version;
        this.headerBlob = headerBlob;
        this.deltaBlob = deltaBlob;
    }

    public long getVersion() {
        return version;
    }

    public ViperConsumer.HeaderBlob getHeaderBlob() {
        return headerBlob;
    }

    public ViperConsumer.Blob getDeltaBlob() {
        return deltaBlob;
    }
}

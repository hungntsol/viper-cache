package net.viper.consumer.read;

import net.viper.consumer.internal.ViperConsumerStatesContext;
import net.viper.core.memory.pooling.buffers.BufferRecycler;
import net.viper.core.states.ViperStatesEngine;

public class ViperStatesReadEngine implements ViperStatesEngine {
    private final ViperConsumerStatesContext consumerStatesContext;
    private final BufferRecycler memoryRecycler;

    private long originRandomizedTag;
    private long currentRandomizedTag;

    public ViperStatesReadEngine(
            ViperConsumerStatesContext consumerStatesContext,
            BufferRecycler bufferRecycler) {
        this.consumerStatesContext = consumerStatesContext;
        this.memoryRecycler = bufferRecycler;
    }

    public long getCurrentRandomizedTag() {
        return this.currentRandomizedTag;
    }

    public long getOriginRandomizedTag() {
        return this.originRandomizedTag;
    }

    public void setCurrentRandomizedTag(long randomizedTag) {
        this.currentRandomizedTag = randomizedTag;
    }

    public void setOriginRandomizedTag(long randomizedTag) {
        this.originRandomizedTag = randomizedTag;
    }

    public ViperSchemaStateRead getSchemaState(String typeName) {
        return this.consumerStatesContext.getSchemaState(typeName);
    }

    public BufferRecycler getMemoryRecycler() {
        return this.memoryRecycler;
    }

    public ViperConsumerStatesContext getConsumerStatesContext() {
        return this.consumerStatesContext;
    }
}

package net.viper.consumer.read;

import net.viper.core.encoding.VarLenHandles;
import net.viper.core.hashing.OpenAddressingHashTable;
import net.viper.core.memory.pooling.buffers.BufferRecycler;
import net.viper.core.memory.store.NativeBytesStore;
import net.viper.core.objects.ViperBlobInput;
import net.viper.core.schema.ViperSchema;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

public class ViperSchemaStateReadImpl implements ViperSchemaStateRead {

    private static final Logger LOG = Logger.getLogger(ViperSchemaStateReadImpl.class.getName());

    protected final ViperSchema schema;
    protected final NativeBytesStore nativeStore;
    protected final OpenAddressingHashTable<Long> keyIndexTable;

    public ViperSchemaStateReadImpl(ViperSchema schema, BufferRecycler bufferRecycler) {
        this.schema = schema;
        this.nativeStore = new NativeBytesStore(bufferRecycler);
        this.keyIndexTable = new OpenAddressingHashTable<>();
    }

    @Override
    public void applyDelta(ViperBlobInput input) throws IOException {
        readDeltaContent(input);
    }

    private void readDeltaContent(ViperBlobInput input) throws IOException {
        readDeltaToAddContent(input);
        readDeltaToRemoveContent(input);
    }

    private void readDeltaToAddContent(ViperBlobInput input) throws IOException {
        // 3. BLOB read schema message size (varint)
        int varintSchemaMessageSize = VarLenHandles.readVarInt(input);
        int inputReadPointer = 0;

        while (inputReadPointer < varintSchemaMessageSize) {
            // 4. BLOB read message size
            int varintMessageLength = VarLenHandles.readVarInt(input);

            byte[] message = new byte[varintMessageLength];
            input.readNBytes(message, varintMessageLength);

            int messageReadPointer = 0;
            // 4.a BLOB read key length
            int msgKeyLength = VarLenHandles.readVarInt(message, messageReadPointer);
            messageReadPointer += VarLenHandles.sizeOfVarInt(msgKeyLength);

            // 4.b BLOB read key
            byte[] msgKey = Arrays.copyOfRange(message, messageReadPointer, messageReadPointer + msgKeyLength);
            messageReadPointer += msgKeyLength;

            // 4.c BLOB read object len
            int msgDataLen = VarLenHandles.readVarInt(message, messageReadPointer);
            messageReadPointer += VarLenHandles.sizeOfVarInt(msgDataLen);

            // 4.d BLOB read object
            byte[] msgData = Arrays.copyOfRange(message, messageReadPointer, messageReadPointer + msgDataLen);

            long dataStorePosition = this.nativeStore.position();
            this.nativeStore.write(msgData);
            this.keyIndexTable.put(msgKey, dataStorePosition);

//            LOG.info(String.format("Put entry in hash-table key = %s", new String(msgKey)));

            inputReadPointer += message.length + VarLenHandles.sizeOfVarInt(varintMessageLength);
        }
    }

    private void readDeltaToRemoveContent(ViperBlobInput input) throws IOException {
        // 5. BLOB read size
        int varintMessageSize = VarLenHandles.readVarInt(input);
        int inputReadPointer = 0;

        while (inputReadPointer < varintMessageSize) {
            // 5.a BLOB read message key len
            int varintMsgKeyLength = VarLenHandles.readVarInt(input);

            // 5.b BLOB read message key
            byte[] msgKey = new byte[varintMsgKeyLength];
            input.readNBytes(msgKey, varintMsgKeyLength);

            this.keyIndexTable.remove(msgKey);

//            LOG.info(String.format("Remove entry in hash-table key = %s", new String(msgKey)));

            inputReadPointer += msgKey.length + VarLenHandles.sizeOfVarInt(varintMessageSize);
        }
    }
}

package net.viper.consumer.read;

import net.viper.core.objects.ViperBlobInput;

import java.io.IOException;
import java.io.InputStream;

public interface ViperBlobReader {
    void applyHeader(ViperBlobInput input) throws IOException;

    void applyHeader(InputStream inStream) throws IOException;

    void applyDelta(ViperBlobInput input) throws IOException;

    void applyDelta(InputStream inStream) throws IOException;
}

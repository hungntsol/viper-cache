package net.viper.consumer.read;

import net.viper.core.encoding.VarLenHandles;
import net.viper.core.memory.MemoryMode;
import net.viper.core.objects.ViperBlobInput;
import net.viper.core.objects.ViperBlobHeaderPlaceholder;
import net.viper.core.schema.ViperSchema;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ViperBlobReaderImpl implements ViperBlobReader {

    private static final Logger LOG = Logger.getLogger(ViperBlobReaderImpl.class.getName());

    private final MemoryMode memoryMode;
    private final ViperStatesReadEngine statesReadEngine;

    public ViperBlobReaderImpl(MemoryMode memoryMode, ViperStatesReadEngine statesReadEngine) {
        this.memoryMode = memoryMode;
        this.statesReadEngine = statesReadEngine;
    }

    @Override
    public void applyHeader(ViperBlobInput input) throws IOException {
        runApplyHeader(input);
    }

    @Override
    public void applyHeader(InputStream inStream) throws IOException {
        ViperBlobInput input = ViperBlobInput.serial(inStream);
        applyHeader(input);
    }

    @Override
    public void applyDelta(ViperBlobInput input) throws IOException {
        runApplyDelta(input);
    }

    @Override
    public void applyDelta(InputStream inStream) throws IOException {
        ViperBlobInput input = ViperBlobInput.serial(inStream);
        applyDelta(input);
    }

    private void runApplyHeader(ViperBlobInput input) throws IOException {
        ensureMemoryMode(input.getMemoryMode());
        long startTime = System.currentTimeMillis();

        ViperBlobHeaderPlaceholder headerPlaceholder = ViperBlobHeaderReaderExtension.readHeader(input);
        Map<Integer, Class<?>> resolverRegistration =
                ViperBlobHeaderReaderExtension.readResolverRegistrations(input);

        this.statesReadEngine.getConsumerStatesContext()
                .registerSchemaModel(headerPlaceholder.getManagedSchemas());
        this.statesReadEngine.getConsumerStatesContext()
                .registerClassResolver(resolverRegistration);

        this.statesReadEngine.setOriginRandomizedTag(headerPlaceholder.getOriginRandomizedTag());
        this.statesReadEngine.setCurrentRandomizedTag(headerPlaceholder.getDestinationRandomizedTag());

        long endTime = System.currentTimeMillis();
        LOG.info(String.format("[DELTA HEADER] read header transition complete; elapse_time = %d ms",
                (endTime - startTime)));
    }

    private void runApplyDelta(ViperBlobInput input) throws IOException {
        ensureMemoryMode(input.getMemoryMode());
        long startTime = System.currentTimeMillis();

        readAndCheckBlobRandomizedTag(input);

        // 1. BLOB read number of schema
        int numSchemas = VarLenHandles.readVarInt(input);
        List<String> schemaClazzNames = new ArrayList<>();

        for (int i = 0; i < numSchemas; i++) {
            String typeName = readSchemaStateDelta(input);
            schemaClazzNames.add(typeName);
            this.statesReadEngine.getMemoryRecycler().advance();
        }

        long endTime = System.currentTimeMillis();
        LOG.info(String.format("[DELTA] schema transition complete; type = %s; elapse_time = %d ms",
                schemaClazzNames, (endTime - startTime)));
    }

    private void ensureMemoryMode(MemoryMode mode) {
        if (!this.memoryMode.equals(mode))
            throw new IllegalStateException(String.format("blob-reader is construct for %s, and blob-input memory " +
                    "mode is %s", this.memoryMode, mode));
    }

    private String readSchemaStateDelta(ViperBlobInput input) throws IOException {
        // 2. BLOB read schema typename
        ViperSchema schema = ViperSchema.readFrom(input);
        ViperSchemaStateRead stateRead = this.statesReadEngine.getSchemaState(schema.getClazzName());

        if (stateRead != null)
            stateRead.applyDelta(input);

        return schema.getClazzName();
    }

    private void readAndCheckBlobRandomizedTag(ViperBlobInput input) throws IOException {
        // 1. BLOB read randomized_tag (long, long)
        long inputOriginRandomizedTag = input.readLong();
        long inputDestinationRandomizedTag = input.readLong();

        if (inputOriginRandomizedTag != this.statesReadEngine.getOriginRandomizedTag()
                && inputDestinationRandomizedTag != this.statesReadEngine.getCurrentRandomizedTag()) {
            throw new IllegalStateException("Attempt to apply delta state that is not origin with state-engine");
        }
    }
}

package net.viper.consumer.read;

import net.viper.core.encoding.VarLenHandles;
import net.viper.core.objects.ViperBlobHeaderPlaceholder;
import net.viper.core.objects.ViperBlobInput;
import net.viper.core.schema.ViperSchema;
import net.viper.exceptions.ViperRuntimeException;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ViperBlobHeaderReaderExtension {
    static ViperBlobHeaderPlaceholder readHeader(ViperBlobInput input) throws IOException {
        ViperBlobHeaderPlaceholder placeholder = new ViperBlobHeaderPlaceholder();

        // version, randomized-tags
        int headerVersion = input.readInt();

        if (headerVersion != ViperBlobHeaderPlaceholder.BLOB_HEADER_VERSION_ID)
            throw new ViperRuntimeException(String.format("Attempt to read an incompatible blob as header, " +
                    "header-blob must have a prefix version id %d", ViperBlobHeaderPlaceholder.BLOB_HEADER_VERSION_ID));

        placeholder.setOriginRandomizedTag(input.readLong());
        placeholder.setDestinationRandomizedTag(input.readLong());

        // schemas
        int schemaWrittenLen = VarLenHandles.readVarInt(input);
        if (schemaWrittenLen != 0) {
            placeholder.setManagedSchemas(readAllSchemas(input));
            skipCompatibilityBytes(input);
        }

        return placeholder;
    }

    static Map<Integer, Class<?>> readResolverRegistrations(ViperBlobInput input)
            throws IOException {
        int size = VarLenHandles.readVarInt(input);
        Map<Integer, Class<?>> registrations = new HashMap<>();

        for (int i = 0; i < size; i++) {
            Integer classId = VarLenHandles.readVarInt(input);
            String clazzTypeName = input.readUTF();
            Class<?> clazz;

            try {
                clazz = Class.forName(clazzTypeName);
            } catch (ClassNotFoundException e) {
                throw new ViperRuntimeException("Could not find and resolve class name = " + clazzTypeName);
            }

            registrations.put(classId, clazz);
        }

        return registrations;
    }

    private static List<ViperSchema> readAllSchemas(ViperBlobInput input) throws IOException {
        int schemaSize = VarLenHandles.readVarInt(input);
        List<ViperSchema> schemas = new ArrayList<>();
        for (int i = 0; i < schemaSize; i++) {
            schemas.add(ViperSchema.readFrom(input));
        }

        return schemas;
    }

    private static Map<String, String> readAllTags(ViperBlobInput input) throws IOException {
        int tagSize = input.readInt();
        Map<String, String> tags = new HashMap<>();
        for (int i = 0; i < tagSize; i++) {
            tags.put(input.readUTF(), input.readUTF());
        }

        return tags;
    }

    private static void skipCompatibilityBytes(ViperBlobInput input) throws IOException {
        int skipLen = VarLenHandles.readVarInt(input);

        while (skipLen > 0) {
            int skipped = (int) input.skipBytes(skipLen);
            if (skipped < 0)
                throw new EOFException("End of input, no bytes can be skip");

            skipLen -= skipped;
        }
    }
}

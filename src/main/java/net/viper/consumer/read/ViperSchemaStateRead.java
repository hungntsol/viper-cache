package net.viper.consumer.read;

import net.viper.core.objects.ViperBlobInput;

import java.io.IOException;

public interface ViperSchemaStateRead {

    void applyDelta(ViperBlobInput input) throws IOException;
}

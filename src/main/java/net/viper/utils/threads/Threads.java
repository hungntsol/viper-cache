package net.viper.utils.threads;

import java.util.Objects;

public class Threads {
    private static final String NAMESPACE = "viper-cache";

    public static Thread ofDaemon(Runnable runnable, Class<?> context, String description) {
        return ofDaemon(runnable, NAMESPACE, context, description);
    }

    public static Thread ofDaemon(Runnable runnable, Class<?> context, String desc, int priority) {
        Thread th = ofDaemon(runnable, NAMESPACE, context, desc);
        th.setPriority(priority);

        return th;
    }

    public static Thread ofDaemon(Runnable runnable, String namespace, Class<?> context, String desc) {
        Objects.requireNonNull(namespace, "Namespace is required not null");
        Objects.requireNonNull(context, "Context is required not null");
        Objects.requireNonNull(desc, "Description is required not null");

        return ofDaemon(runnable, buildMetadata(namespace, context, desc));
    }

    public static Thread ofDaemon(Runnable runnable, String name) {
        Objects.requireNonNull(runnable, "Runnable task is required not null");
        Objects.requireNonNull(name, "Name is required not null");

        Thread th = new Thread(runnable, name);
        th.setDaemon(true);

        return th;
    }

    private static String buildMetadata(String namespace, Class<?> context, String desc) {
        return namespace +
                "." +
                context.getSimpleName() +
                "." +
                desc;
    }
}

package net.viper.utils.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class ViperWorkerExecutor extends ThreadPoolExecutor {
    private static final Logger LOG = Logger.getLogger(ViperWorkerExecutor.class.getName());
    private static final String DEFAULT_THREAD_DESCRIPTION = "viper-executor";

    private final List<Future<?>> futures = new ArrayList<>();

    protected ViperWorkerExecutor(int numOfThreads, ThreadFactory threadFactory) {
        super(numOfThreads, numOfThreads, 300, TimeUnit.SECONDS, new LinkedBlockingQueue<>(),
                threadFactory);
    }

    public ViperWorkerExecutor(int numOfThreads, Class<?> context, String description, int priority) {
        this(numOfThreads, r -> Threads.ofDaemon(r, context, description, priority));
    }

    public static ViperWorkerExecutor ofThreadPerCpu(
            int threadPerCpu,
            Class<?> context,
            String description,
            int priority) {
        return new ViperWorkerExecutor(Runtime.getRuntime().availableProcessors() * threadPerCpu,
                context,
                description,
                priority);
    }

    public static ViperWorkerExecutor ofThreadPerCpu(
            int threadPerCpu,
            Class<?> context,
            String description) {
        return ofThreadPerCpu(threadPerCpu, context, description, Thread.NORM_PRIORITY);
    }

    public static ViperWorkerExecutor ofProcessor(Class<?> context, String description) {
        return ofThreadPerCpu(1, context, description);
    }

    public static ViperWorkerExecutor of(int numOfThreads, Class<?> context, String description) {
        return new ViperWorkerExecutor(numOfThreads, context, description, Thread.NORM_PRIORITY);
    }

    public static ViperWorkerExecutor of(int numOfThreads, Class<?> context) {
        return of(numOfThreads, context, DEFAULT_THREAD_DESCRIPTION);
    }

    @Override
    public void execute(Runnable command) {
        if (command instanceof RunnableFuture)
            super.execute(command);
        else
            super.execute(newTaskFor(command, Boolean.TRUE));
    }

    @Override
    protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        final RunnableFuture<T> task = super.newTaskFor(callable);
        this.futures.add(task);

        return task;
    }

    @Override
    protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
        final RunnableFuture<T> task = super.newTaskFor(runnable, value);
        this.futures.add(task);

        return task;
    }

    public void nonInterruptWait() {
        shutdown();
        if (!isTerminated()) {
            try {
                awaitTermination(1, TimeUnit.HOURS);
            } catch (InterruptedException ex) {
                LOG.warning(String.format("Task interrupt, stack-trace: %s", ex));
            }
        }
    }

    /**
     * Wait all submitted task to complete. ThreadPool is shutdown.
     */
    public void waitAll() throws InterruptedException, ExecutionException {
        nonInterruptWait();
        for (Future<?> f : this.futures)
            f.get();
    }

    /**
     * Wait all submitted task to complete, ThreadPool is not shutdown.
     */
    public void flushCurrentTasks() throws InterruptedException, ExecutionException {
        for (Future<?> f : this.futures)
            f.get();

        this.futures.clear();
    }
}

package net.viper.utils.types;

public class RefComparisons {
    public static <T> boolean compareEquality(T a, T b) {
        if (a == b)
            return true;

        return a != null && a.equals(b);
    }
}

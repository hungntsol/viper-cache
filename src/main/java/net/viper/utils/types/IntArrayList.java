package net.viper.utils.types;

import java.util.Arrays;

public class IntArrayList {
    private int[] backs;
    private int size;

    public static IntArrayList DEFAULT_INSTANCE = new IntArrayList(32);

    public IntArrayList() {
        this(32);
    }

    public IntArrayList(int initSize) {
        this.backs = new int[initSize];
        this.size = 0;
    }

    public int size() {
        return this.size;
    }

    public int get(int i) {
        return this.backs[i];
    }

    public void set(int i, int value) {
        if (i >= this.size)
            throw new ArrayIndexOutOfBoundsException(String.format("index=%s is out of bound", i));

        this.backs[i]= value;
    }

    public void pushBack(int value) {
        growSize();
        this.backs[this.size++] = value;
    }

    public void clear() {
        this.size = 0;
    }

    public void sort() {
        Arrays.sort(this.backs, 0, this.size);
    }

    private void growSize() {
        if (this.backs.length == this.size)
            this.backs = Arrays.copyOf(this.backs, this.size * 3 / 2);
    }
}

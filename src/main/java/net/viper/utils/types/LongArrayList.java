package net.viper.utils.types;

import java.util.Arrays;

public class LongArrayList {
    private long[] backs;
    private int size;

    public static LongArrayList DEFAULT_INSTANCE = new LongArrayList(32);

    public LongArrayList(int size) {
        this.size = 0;
        this.backs = new long[size];
    }

    public int size() {
        return this.size;
    }

    public long get(int i) {
        if (i >= this.size)
            throw new ArrayIndexOutOfBoundsException(String.format("index=%s is out of bound of LongArrayList", i));

        return this.backs[i];
    }

    public void set(int i, long value) {
        if (i >= this.size)
            throw new ArrayIndexOutOfBoundsException(String.format("index=%s is out of bound of LongArrayList", i));

        this.backs[i] = value;
    }

    public void pushBack(long value) {
        growCapacity();
        this.backs[this.size++] = value;
    }

    public void sort() {
        Arrays.sort(this.backs, 0, this.size);
    }

    public void clear() {
        this.size = 0;
    }

    private void growCapacity() {
        if (this.backs.length == this.size)
            this.backs = Arrays.copyOf(this.backs, this.size * 3 / 2);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof LongArrayList other))
            return false;

        if (other.size() != size())
            return false;

        for (int i = 0; i < other.size(); i++) {
            if (other.get(i) != get(i))
                return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.size;
        result = 37 * result + Arrays.hashCode(this.backs);

        return result;
    }
}

package net.viper.utils.logic;

import java.util.Random;

public class Randoms {
    public static int randInt() {
        return new Random().nextInt();
    }

    public static int randInt(int low, int high) {
        return new Random().nextInt(low, high);
    }

    public static long randLong() {
        return new Random().nextLong();
    }

    public static long randLong(int low, int high) {
        return new Random().nextLong(low, high);
    }

    public static byte[] randBytes(int len) {
        byte[] result = new byte[len];
        new Random().nextBytes(result);

        return result;
    }
}

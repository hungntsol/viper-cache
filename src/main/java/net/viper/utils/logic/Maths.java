package net.viper.utils.logic;

public class Maths {
    public static int min(int... elements) {
        assert elements.length > 0;

        int ans = elements[0];
        for (int el : elements)
            ans = Math.min(el, ans);

        return ans;
    }

    public static long min(long... elements) {
        assert elements.length > 0;

        long ans = elements[0];
        for (long el : elements)
            ans = Math.min(el, ans);

        return ans;
    }
}

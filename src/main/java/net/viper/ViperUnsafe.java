package net.viper;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.logging.Logger;

public class ViperUnsafe {
    private static final Logger LOG = Logger.getLogger(ViperUnsafe.class.getName());

    private static final Unsafe unsafe;

    static {
        Field theUnsafe;
        Unsafe usf = null;
        try {
            theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            usf = (Unsafe) theUnsafe.get(null);
            unsafe = usf;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static Unsafe unsafe() {
        return unsafe;
    }
}

package net.viper;

public class Jvm {
    public static final String SYSTEM_TEMPDIR = System.getProperty("java.io.tmpdir");
    public static final int MAX_SINGLE_BUFFER_BYTE_CAPACITY = (1 << 30);

    public static final String FILE_MODE_READ_ONLY = "r";
    public static final String FILE_MODE_READ_WRITE = "rw";

    public static final int DEFAULT_HASHCODE_PRIME_NUMBER = 37;
}
